いまここマップ
==============

[![Run Status](https://api.shippable.com/projects/57b0172e88ee4b0e00ebf270/badge?branch=master)](https://app.shippable.com/projects/57b0172e88ee4b0e00ebf270)
[![Coverage Badge](https://api.shippable.com/projects/57b0172e88ee4b0e00ebf270/coverageBadge?branch=master)](https://app.shippable.com/projects/57b0172e88ee4b0e00ebf270)

## 概要

地図をみんなで共有して今いる場所を記録するサービスです。
https://imakoko-map.com


## 機能一覧

- 現在位置の共有機能
- 地図へのマーキング機能


## 利用技術

### システム構成

|種類                  |利用技術  |
|:---------------------|:---------|
|言語(クライアント)    |JavaScript|
|言語(サーバ)          |Java 8    |
|フレームワーク(CSS）  |Bootstrap3|
|フレームワーク(サーバ)|SpringBoot|
|DBスキーマ管理        |flyway    |
|ビルドツール          |Gradle    |

### 本番サーバ

|種類            |利用技術  |
|:---------------|:---------|
|OS              |CentOS6   |
|DB              |PostgreSQL|
|Webサーバ       |Apache    |
|サーバの公開方法|さくらVPS |

### その他

|種類  |利用技術  |
|:-----|:---------|
|CI    |Shippable |


## 開発環境の作り方

1. インターネットに接続できるコンピュータを用意。
2. 下記をインストール。
    * JDK8
    * Git
    * PostgreSQL
3. ソースコードをGitでクローン。
4. データベースを準備。
    1. 開発用のデータベースを作成。下記設定でアクセスできるようにしておくこと。
        * ユーザー : postgres
        * パスワード : postgres
        * ポート : 5432
    2. データベースへの接続設定を環境変数に追加。
        * `DB_URL=postgres://ユーザ名:パスワード@127.0.0.1:5432/DB名`
        * `DB_ENABLESSL=false`
    3. ソースコードのディレクトリで下記コマンドを実行してDBにテーブルを作成。
        * `./gradlew flywayMigrate`
5. ビルドできるか確認。
    1. ソースコードのディレクトリで下記コマンドを実行。
        * `./gradlew clean build`
    2. 次のメッセージが表示されればOK。
        * `BUILD SUCCESSFUL`
6. 起動できるか確認。
    1. ソースコードのディレクトリで下記コマンドを実行。
        * `./gradlew bootRun`
    2. Webブラウザで次のURLに接続してトップページが表示されればOK。
        * http://localhost:8080/imakoko


## リンク

* 本番環境 :  https://imakoko-map.com
* テスト環境 : https://imakoko-stamp.herokuapp.com/imakoko


## 素材

* WordPressにGoogle Maps API V3! : http://waox.main.jp/news/?page_id=229 

