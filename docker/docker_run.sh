#!/bin/sh

hostPort=8080
if [ $# -eq 1 ]; then
    hostPort=${1}
fi

cd `dirname "${0}"`/../
current=`pwd`

sudo docker run -d -p ${hostPort}:8080 -v $current/build/libs:/host imakoko
