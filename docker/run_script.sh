#!/bin/sh

if [ -e /host/imakoko.jar ]; then
    echo "update imakoko.jar"
    cp /host/imakoko.jar /opt/spring/
else
    echo "skip update imakoko.jar"
fi

java -jar /opt/spring/imakoko.jar --db.url=postgres://postgres:postgres@172.17.0.1:5432/imakoko --server.contextPath=/
