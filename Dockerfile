FROM java:8
ADD ./build/libs/imakoko.jar /opt/spring/imakoko.jar
ADD ./docker/run_script.sh /opt/spring/run_script.sh
EXPOSE 8080
WORKDIR /opt/spring

ENTRYPOINT ["sh", "run_script.sh"]
