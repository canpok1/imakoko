#!/bin/sh

echo "****************************************"
echo "start build"
echo "****************************************"
echo -n "Do you want to build? [y/n] :"
read SHOULD_BUILD

if [ $SHOULD_BUILD = "y" ]; then
    ./gradlew clean stage
else
    echo "skip build"
fi

echo "****************************************"
echo "start deploy"
echo "****************************************"

echo -n "What is remote host name? :"
read HOST

echo -n "What is ssh port number? :"
read PORT

echo -n "What is remote user name? :"
read USER

echo "$HOST:$PORT ansible_ssh_user=$USER" > config/ansible/hosts
cp -f build/libs/imakoko.jar config/ansible/roles/deploy/files/

ansible-playbook --private-key=/ssh/id_rsa -i config/ansible/hosts config/ansible/setup.yml -K
