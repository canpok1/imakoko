package jp.gr.java_conf.ktnet.imakoko.it.api;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.gr.java_conf.ktnet.imakoko.it.IntegrationTestBase;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import org.dbunit.dataset.ITable;
import org.junit.After;
import org.junit.Ignore;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

/**
 * エンドポイントが/api/stampsのAPIに対する結合テストを行うクラス.
 * @author tanabe
 */
@RunWith(Enclosed.class)
public class ApiStampsTest {

  private static final String EMPTY_ALL
      = "./src/test/resources/records/EmptyAll.xml";
  private static final String MANY_STAMPSHEETS
      = "./src/test/resources/records/it/apistamps/ManyStampsheets.xml";
  
  @RunWith(Theories.class)
  public static class スタンプ情報を更新するリクエストを投げた場合 extends IntegrationTestBase {
    
    @Data
    @RequiredArgsConstructor
    public static class Fixture {
      private final String testDataFile;
      private final Stamp requestParam;
      private final int responseStatus;
      private final int createStampCount;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        // 存在しないスタンプIDでリクエスト
        new Fixture(MANY_STAMPSHEETS,
            new Stamp("1-1", "1", "名前", "35.690553", "139.699579", 0, null, null), 200, 1),
        // 存在するスタンプIDでリクエスト
        new Fixture(MANY_STAMPSHEETS,
            new Stamp("2-1", "2", "名前", "35.690553", "139.699579", 0, null, null), 200, 0)
    };
    
    @After
    public void tearDown() throws Exception {
      this.tearDownDatabase();
    }
    
    @Theory
    public void レスポンスが想定通り(Fixture fixture) throws Exception {
      // 初期設定
      this.setupMvc();
      this.setupDatabase(fixture.testDataFile);
      
      int beforeRecordCount = fetchTable("stamps").getRowCount();
      
      ObjectMapper mapper = new ObjectMapper();
      
      // リクエスト投げる
      ResultActions resultActions = this.mvc.perform(
          post("/api/stamps")
          .contentType(MediaType.APPLICATION_JSON)
          .content(mapper.writeValueAsString(fixture.requestParam))
      );
    
      // レスポンスの検証
      resultActions
        .andExpect(status().is(fixture.responseStatus))
        .andExpect(jsonPath("$.body.id", is(not(""))));
      
      // DBに登録されてるか検証
      ITable table = fetchTable("stamps");
      assertThat(table.getRowCount(), is(beforeRecordCount + fixture.getCreateStampCount()));
    }
  }
  
  @RunWith(Theories.class)
  public static class スタンプ情報を取得するリクエストを投げた場合 extends IntegrationTestBase {
    
    @Data
    @RequiredArgsConstructor
    public static class Fixture {
      private final String testDataFile;
      private final String stampSheetId;
      private final int responseStatus;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        // スタンプ0件のマップを指定
        new Fixture(MANY_STAMPSHEETS, "1", 200),
        // スタンプ複数件のマップを指定
        new Fixture(MANY_STAMPSHEETS, "2", 200),
    };
    
    @After
    public void tearDown() throws Exception {
      this.tearDownDatabase();
    }
    
    @Theory
    public void レスポンスが想定通り(Fixture fixture) throws Exception {
      // 初期設定
      this.setupMvc();
      this.setupDatabase(fixture.testDataFile);
      
      // リクエスト投げる
      ResultActions resultActions = this.mvc.perform(
          get("/api/stamps/" + fixture.getStampSheetId())
      );
    
      // レスポンスの検証
      resultActions
        .andExpect(status().is(fixture.responseStatus));
    }
  }
}
