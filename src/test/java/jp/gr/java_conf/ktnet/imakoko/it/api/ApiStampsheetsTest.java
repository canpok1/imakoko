package jp.gr.java_conf.ktnet.imakoko.it.api;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import jp.gr.java_conf.ktnet.imakoko.it.IntegrationTestBase;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import org.dbunit.dataset.ITable;
import org.junit.After;
import org.junit.Ignore;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.ResultActions;

/**
 * エンドポイントが/api/stampsheetsのAPIに対する結合テストを行うクラス.
 * @author tanabe
 */
@RunWith(Enclosed.class)
public class ApiStampsheetsTest {

  private static final String EMPTY_ALL
      = "./src/test/resources/records/EmptyAll.xml";
  
  @RunWith(Theories.class)
  public static class スタンプシートを作成するリクエストを投げた場合 extends IntegrationTestBase {
    
    @Data
    @RequiredArgsConstructor
    public static class Fixture {
      private final String testDataFile;
      private final int responseStatus;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(EMPTY_ALL, 200)
    };
    
    @After
    public void tearDown() throws Exception {
      this.tearDownDatabase();
    }
    
    @Theory
    public void レスポンスが想定通り(Fixture fixture) throws Exception {
      // 初期設定
      this.setupMvc();
      this.setupDatabase(fixture.testDataFile);
      
      // リクエスト投げる
      ResultActions resultActions = this.mvc.perform(
          post("/api/stampsheets")
          );
    
      // レスポンスの検証
      resultActions
        .andExpect(status().is(fixture.responseStatus));
      
      // DBに登録されてるか検証
      ITable table = fetchTable("stamp_sheets");
      assertThat(table.getRowCount(), is(1));
    }
  }
}
