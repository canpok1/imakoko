CREATE TABLE imakoko.stamps (
    id varchar(36) NOT NULL,
    stamp_sheet_id varchar(36) NOT NULL REFERENCES stamp_sheets(id),
    name varchar(64) NOT NULL,
    latitude text NOT NULL,
    longitude text NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    PRIMARY KEY (id)
);
COMMENT ON TABLE imakoko.stamps IS 'スタンプテーブル';
COMMENT ON COLUMN imakoko.stamps.id IS 'ID';
COMMENT ON COLUMN imakoko.stamps.stamp_sheet_id IS 'シートID';
COMMENT ON COLUMN imakoko.stamps.name IS '名前';
COMMENT ON COLUMN imakoko.stamps.latitude IS '緯度';
COMMENT ON COLUMN imakoko.stamps.longitude IS '軽度';
COMMENT ON COLUMN imakoko.stamps.created IS '作成日時';
COMMENT ON COLUMN imakoko.stamps.updated IS '更新日時';
