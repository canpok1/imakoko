CREATE TABLE imakoko.stamp_sheets (
    id varchar(36) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    PRIMARY KEY (id)
);
COMMENT ON TABLE imakoko.stamp_sheets IS 'スタンプシートテーブル';
COMMENT ON COLUMN imakoko.stamp_sheets.id IS 'ID';
COMMENT ON COLUMN imakoko.stamp_sheets.created IS '作成日時';
COMMENT ON COLUMN imakoko.stamp_sheets.updated IS '更新日時';
