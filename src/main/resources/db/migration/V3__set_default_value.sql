ALTER TABLE imakoko.stamp_sheets ALTER COLUMN created SET DEFAULT NOW();
ALTER TABLE imakoko.stamp_sheets ALTER COLUMN updated SET DEFAULT NOW();

ALTER TABLE imakoko.stamps ALTER COLUMN created SET DEFAULT NOW();
ALTER TABLE imakoko.stamps ALTER COLUMN updated SET DEFAULT NOW();
