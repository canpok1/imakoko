'use strict'
var namespaceIndex = (function() {
    var stampSheetApi = new StampSheetApi();

    return {
        /**
        * 今すぐ始めるボタン押下時処理。
        */
        createButtonClicked: function() {
            var response = stampSheetApi.create().done(function(param) {
                location.href= CONTEXT + "stampsheet?id=" + param.id;
            });
        },

        initialize: function() {
            $('.js-create-btn').click(function() {
                var response = stampSheetApi.create()
                    .done(function(param) {
                        $('#mySuccessModal').on('hide.bs.modal', function(event) {
                            location.href= CONTEXT + "stampsheet?id=" + param.id;
                        });
                        $('#mySuccessModal').modal('show');
                    }).fail(function() {
                        $('#myFailureModal').modal('show');
                    });
            });
        }
    };
});
namespaceIndex().initialize();
