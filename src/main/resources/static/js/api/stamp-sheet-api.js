/*global ApiBase*/

'use strict'
var StampSheetApi = (function ($) {
    function StampSheetApi() {
        ApiBase.apply(this, arguments);
    }

    $.extend(true, StampSheetApi.prototype, ApiBase.prototype);

    StampSheetApi.prototype.create = function() {
        var ENDPOINT = CONTEXT + 'api/stampsheets';
        var _api_endpoint = this.api_host() + ENDPOINT;
        return this.request_post(_api_endpoint);
    };

    return StampSheetApi;
}(jQuery));
