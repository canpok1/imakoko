/*global ApiBase*/

'use strict'
var StampApi = (function ($) {
    function StampApi() {
        ApiBase.apply(this, arguments);
    }

    $.extend(true, StampApi.prototype, ApiBase.prototype);

    /**
    * スタンプを保存します。
    * @param {Object} stamp スタンプ情報。
    * @return {Object} レスポンス。
    *     { id: 保存したスタンプのID }
    */
    StampApi.prototype.saveStamp = function(stamp) {
        var ENDPOINT = CONTEXT + 'api/stamps';
        var _api_endpoint = this.api_host() + ENDPOINT;
        return this.request_post(_api_endpoint, stamp);
    };

    /**
    * 指定のスタンプシートIDの全スタンプを取得します。
    * @param {string} stampSheetId スタンプシートID。
    * @return {Object} レスポンス。
    *     [{スタンプ情報}, {スタンプ情報}, ...]
    */
    StampApi.prototype.fetchStampByStampSheetId = function(stampSheetId) {
        var ENDPOINT = CONTEXT + 'api/stamps/' + stampSheetId;
        var _api_endpoint = this.api_host() + ENDPOINT;
        return this.request_get(_api_endpoint);
    };

    return StampApi;
}(jQuery));
