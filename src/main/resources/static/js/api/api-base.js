'use strict'
var ApiBase = (function ($) {
    var _api_host;
    var _promise;

    function ApiBase() {
        _api_host = location.origin;
    }

    function isObject(data){
        if(typeof data === "object" && data !== null){
            return true;
        } else {
            return false;
        }
    }

    ApiBase.prototype.api_host = function() {
        return _api_host;
    };

    _promise = function(request) {
        var _deferred = new $.Deferred;
        request
            .done(function (_xhr) {
                _deferred.resolve(_xhr['body']);
            })
            .fail(function () {
                _deferred.reject();
            });
        return _deferred.promise();
    };

    ApiBase.prototype.request_get = function(url, data) {
        if(!isObject(data)) { data = {}; }
        return _promise($.ajax({
            type: 'get',
            url: url,
            data: data
        }));
    };

    ApiBase.prototype.request_post = function(url, data) {
        if(!isObject(data)) { data = {}; }
        return _promise($.ajax({
            type: 'post',
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }));
    };

    ApiBase.prototype.request_put = function(url, data) {
        if(!isObject(data)) { data = {}; }
        return _promise($.ajax({
            type: 'put',
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        }));
    };

    return ApiBase;
}(jQuery));
