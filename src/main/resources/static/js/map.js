/* global google */
/* global ApiBase */
/* global StampApi */
'use strict'
var Map = (function ($) {

    var mapObject;

    /**
    * Mapを初期化します。
    * successCallbackの呼び出し時、第一引数には緯度が第二引数には経度が渡されます。
    * @param {Object} mapCanvas Mapの描画先であるjQueryオブジェクト.
    * @param {string} latitude 地図の中心の緯度
    * @param {string} longitude 地図の中心の経度
    * @param {navigator} navigatorObj navigatorオブジェクト
    * @param {function} successCallback 位置情報取得成功時に呼び出される関数
    * @param {function} failurecallback 位置情報取得失敗時に呼び出される関数
    */
    function Map (
            mapCanvas,
            latitude,
            longitude,
            navigatorObj,
            successCallback,
            failureCallback
    ) {
        if (latitude && longitude) {
            mapObject = new LeafletMap(
                                mapCanvas,
                                latitude,
                                longitude,
                                navigatorObj,
                                successCallback,
                                failureCallback);
        } else {
            mapObject = new LeafletMap(
                                mapCanvas,
                                35.690553,
                                139.699579,
                                navigatorObj,
                                successCallback,
                                failureCallback);
        }
    }

    Map.prototype.loadCurrentPosition = function(navigatorObj) {
        mapObject.loadCurrentPosition(navigatorObj);
    };


    /**
    * 現在位置を取得します
    * @return {Object} 現在位置。次の形式。
    *   {latitude: 緯度, longitude: 経度}
    */
    Map.prototype.fetchCurrentPosition = function() {
        return mapObject.fetchCurrentPosition();
    };


    /**
    * スタンプのマーカーを更新します。
    * @param {Object} stamps スタンプ情報
    */
    Map.prototype.updateStampMarker = function(stamps) {
        mapObject.updateStampMarker(stamps);
    };


    /**
    * 指定のスタンプのコメントウィンドウを開きます。
    * @param {string} stampId スタンプID
    */
    Map.prototype.openStampCommentWindow = function(stampId) {
        mapObject.openStampCommentWindow(stampId);
    };

    /**
    * 指定の緯度経度が中心になるように表示します。
    * @param {string} latitude 緯度
    * @param {string} longitude 経度
    */
    Map.prototype.setCenter = function(latitude, longitude) {
        mapObject.setCenter(latitude, longitude);
    };

    /**
    * 中心の緯度経度を取得します。
    * @param {object} 緯度経度。次の形式。
    *    { latitude: 緯度, longitude: 経度 }
    */
    Map.prototype.getCenter = function() {
        return mapObject.getCenter();
    };

    return Map;
}(jQuery));

