/* global google */
/* global ApiBase */
/* global StampApi */
'use strict'
var LeafletMap = (function ($) {

    var mapObject;
    var marker;
    var stampInfo = {};

    /**
    * Mapを初期化します。
    * successCallbackの呼び出し時、第一引数には緯度が第二引数には経度が渡されます。
    * @param {Object} mapObject Mapの描画先であるjQueryオブジェクト.
    * @param {string} latitude 地図の中心の緯度
    * @param {string} longitude 地図の中心の経度
    * @param {navigator} navigatorObj navigatorオブジェクト
    * @param {function} successCallback 位置情報取得成功時に呼び出される関数
    * @param {function} failurecallback 位置情報取得失敗時に呼び出される関数
    */
    function LeafletMap(
            mapCanvas,
            latitude,
            longitude,
            navigatorObj,
            successCallback,
            failureCallback
    ) {
        console.log(
                "LeafletMap 呼び出し[" + latitude + "," + longitude + "]"
                );

        mapObject = L.map(mapCanvas).setView([latitude, longitude], 13);

        //OSMレイヤー追加
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
             {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
                maxZoom: 18
             }
        ).addTo(mapObject);

        var pulseIcon = L.icon.pulse({
                            iconSize:[10, 10],
                            color: 'blue'
                        })
        marker = L.marker(
                    [latitude, longitude],
                    { icon: pulseIcon }
                 ).addTo(mapObject);

        L.easyButton({
            position: 'topright',
            states: [
                {
                    icon: '<span class="glyphicon glyphicon-cog"></span>',
                    onClick: function(btn, map) { $('#optionModal').modal(); }
                }
            ]
        }).addTo(mapObject);
        L.easyButton({
            position: 'topright',
            states: [
                {
                    icon: '<span class="glyphicon glyphicon-th-list"></span>',
                    onClick: function(btn, map) { $('#stampListModal').modal(); }
                }
            ]
        }).addTo(mapObject);

        geolocationInit(navigatorObj, marker, successCallback, failureCallback);
    }

    /**
    * 位置情報関連を初期化します。
    * successCallbackの呼び出し時、第一引数には緯度が第二引数には経度が渡されます。
    * @param {Object} navigatorObj navigatorオブジェクト
    * @param {Object} marker 自分の位置用マーカーオブジェクト
    * @param {function} successCallback 位置情報取得成功時に呼び出される関数
    * @param {function} failureCallback 位置情報取得失敗時に呼び出される関数
    */
    function geolocationInit(navigatorObj, marker, successCallback, failureCallback) {
        if (navigatorObj.geolocation) {
            console.log("位置情報取得可能");
            navigator.geolocation.watchPosition(
                    // 成功
                    function(position) {
                        if (!position) {
                            return;
                        }
                        if (!position.coords) {
                            return;
                        }
                        var latlng = new L.LatLng(
                                            position.coords.latitude,
                                            position.coords.longitude);
                        if (marker) {
                            marker.setLatLng(latlng);
                        }
                        if ($("[name=trace]").prop("checked")) {
                            mapObject.panTo(latlng);
                        }
                        var positionInfo = "緯度: " + position.coords.latitude + ","
                                         + "経度: " + position.coords.longitude;
                        console.log("位置取得成功[" + positionInfo + "]");
                        $('#latlng-label').text(latlng.toString());

                        if (successCallback) {
                            successCallback(
                                    position.coords.latitude,
                                    position.coords.longitude);
                        }
                    },
                    // 失敗
                    function(err) {
                        console.log("位置取得失敗[" + err.message + "]");
                        var reason = {
                            0: '原因不明のエラーが発生しました…。',
                            1: '位置情報の取得が許可されませんでした…。',
                            2: '電波状況などで位置情報が取得できませんでした…。',
                            3: '位置情報の取得に時間がかかり過ぎてタイムアウトしました…。',
                        }
                        console.log(reason[err.code]);

                        if (failureCallback) {
                            failureCallback();
                        }
                    },
                    // オプション
                    {
                        enableHighAccuracy: true,
                    }
            );
        } else {
            console.log("位置情報取得不可能");
        }
    }


    LeafletMap.prototype.loadCurrentPosition = function(navigatorObj) {

        console.log("loadCurrentPosition 呼び出し");

        if (!navigatorObj.geolocation) {
            console.log("位置情報取得不可能");
            return;
        }

        navigatorObj.geolocation.getCurrentPosition(
                    // 成功
                    function(position) {
                        if (!position) {
                            return;
                        }
                        if (!position.coords) {
                            return;
                        }
                        var latlng = new L.LatLng(
                                            position.coords.latitude,
                                            position.coords.longitude);
                        if (marker) {
                            marker.setLatlng(latlng);
                        }
                        if ($("[name=trace]").prop("checked")) {
                            mapObject.panTo(latlng);
                        }
                        var positionInfo = "緯度: " + position.coords.latitude + ","
                                         + "経度: " + position.coords.longitude;
                        console.log("位置取得成功[" + positionInfo + "]");
                        $('#latlng-label').text(latlng.toString());
                    },
                    // 失敗
                    function(err) {
                        console.log("位置取得失敗[" + err.message + "]");
                        var reason = {
                            0: '原因不明のエラーが発生しました…。',
                            1: '位置情報の取得が許可されませんでした…。',
                            2: '電波状況などで位置情報が取得できませんでした…。',
                            3: '位置情報の取得に時間がかかり過ぎてタイムアウトしました…。',
                        }
                        console.log(reason[err.code]);
                    },
                    // オプション
                    {
                        enableHighAccuracy: true,
                    }
        );
    };


    /**
    * 現在位置を取得します
    * @return {Object} 現在位置。次の形式。
    *   {latitude: 緯度, longitude: 経度}
    */
    LeafletMap.prototype.fetchCurrentPosition = function() {

        console.log("fetchCurrentPosition 呼び出し");

        if (marker) {
            return {
                latitude: marker.getLatLng().lat,
                longitude: marker.getLatLng().lng
            };
        }
        return {latitude: 0, longitude: 0};
    };


    /**
    * スタンプのマーカーを更新します。
    * @param {Object} stamps スタンプ情報
    */
    LeafletMap.prototype.updateStampMarker = function(newStampList) {

        console.log("updateStampMarker 呼び出し");

        newStampList.forEach(function(newStamp) {
            var position = new L.LatLng(
                                    newStamp.latitude,
                                    newStamp.longitude);
            if (newStamp.id in stampInfo) {
                // すでにスタンプが存在
                stampInfo[newStamp.id].marker.setLatLng(position);
                stampInfo[newStamp.id].marker.bindPopup(newStamp.name);
                console.log("スタンプ位置更新");
            } else {
                // 新スタンプを作成
                var option = {};
                if (newStamp.icon_type) {
                    var iconUrl;
                    if (newStamp.icon_type === 1) {
                        iconUrl = CONTEXT + "image/red-pushpin.png";
                    } else if (newStamp.icon_type === 2) {
                        iconUrl = CONTEXT + "image/grn-pushpin.png";
                    } else if (newStamp.icon_type === 3) {
                        iconUrl = CONTEXT + "image/blue-pushpin.png";
                    }
                    if (iconUrl) {
                        option.icon = L.icon({
                                          iconUrl: iconUrl,
                                          iconSize: [32, 32],
                                          iconAnchor: [10, 32],
                                          popupAnchor: [10, -32]
                                      });
                    }
                }

                stampInfo[newStamp.id] = {
                    marker: new L.marker(position, option)
                };
                stampInfo[newStamp.id].marker.bindPopup(newStamp.name);
                stampInfo[newStamp.id].marker.addTo(mapObject);
                console.log("スタンプ作成");
            }
        });
    };


    /**
    * 指定のスタンプのコメントウィンドウを開きます。
    * @param {string} stampId スタンプID
    */
    LeafletMap.prototype.openStampCommentWindow = function(stampId) {

        console.log("openStampCommentWindow 呼び出し");

        stampInfo[stampId].marker.openPopup();
    }

    /**
    * 指定の緯度経度が中心になるように表示します。
    * @param {string} latitude 緯度
    * @param {string} longitude 経度
    */
    LeafletMap.prototype.setCenter = function(latitude, longitude) {

        console.log("setCenter 呼び出し");

        mapObject.panTo(L.latLng(latitude, longitude));
    }

    /**
    * 中心の緯度経度を取得します。
    * @param {object} 緯度経度。次の形式。
    *    { latitude: 緯度, longitude: 経度 }
    */
    LeafletMap.prototype.getCenter = function() {
        console.log("getCenter 呼び出し");
        return {
            latitude: mapObject.getCenter().lat,
            longitude: mapObject.getCenter().lng
        };
    }

    return LeafletMap;
}(jQuery));

