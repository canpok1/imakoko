/* global google */
/* global ApiBase */
/* global StampApi */
/* global Map */
'use strict'
var namespaceStampsheet = (function(navigator) {

    var stampApi;
    var map;

    /**
    * 現在位置を記録します
    * @param {String} stampSheetId スタンプシートID
    * @param {String} latitude 緯度
    * @param {String} longitude 経度
    * @param {String} message メッセージ
    * @param {number} iconType アイコン種別
    * @param {function} successcallback 記録成功時のコールバック関数
    */
    function savePosition(stampSheetId, latitude, longitude, message, iconType, successCallback) {
        var param = {
            id: getMyStampId(iconType),
            stamp_sheet_id: stampSheetId,
            name: message,
            latitude: latitude,
            longitude: longitude,
            icon_type: iconType,
        };
        stampApi.saveStamp(param).done(function(body) {
            var stampId = body.id;
            setMyStampId(stampId, iconType);
            console.log("サーバに位置を送信[緯度:" + latitude + ", 経度:" + longitude + "][" + stampId + "]");
            successCallback(stampId);
        }).fail(function() {
            console.log("サーバへの位置送信に失敗[緯度:" + latitude + ", 経度:" + longitude + "]");
        });
    }

    /**
    * スタンプ情報を最新のものに更新します。
    */
    function updateStamp(stampSheetId, activeStampId) {
        stampApi.fetchStampByStampSheetId(stampSheetId)
            .done(function(stamps) {
                console.log("スタンプ取得成功[" + stamps.length + "件]");
                map.updateStampMarker(stamps);

                // スタンプIDの指定があれば、そのスタンプのコメントを表示
                if (activeStampId) {
                    map.openStampCommentWindow(activeStampId);
                }

                updateImakokoTable(stamps);
                updateMarkerTable(stamps);

            }).fail(function() {
                console.log("スタンプ取得失敗");
            });
    }

    /**
    * スタンプ情報をいまここ一覧に表示します。
    */
    function updateImakokoTable(stamps) {
        var tbody = $('#stamp-table');
        tbody.find("tr:gt(0)").remove();
        var count = 0;
        stamps.forEach(function(stamp) {
            if (stamp.icon_type !== 0) {
                return;
            }
            var updated = dateToString(new Date(stamp.updated));
            var button = "<button "
                       + "class=\"btn btn-primary\" "
                       + "data-dismiss=\"modal\" "
                       + "onClick=\"namespaceStampsheet.viewStampButtonClick("
                       +   "'" + stamp.id + "',"
                       +   "'" + stamp.latitude + "',"
                       +   "'" + stamp.longitude + "'"
                       + ");\""
                       + "><i class='glyphicon glyphicon-eye-open'></i></button>";
            var icon = "<span "
                     + "class='glyphicon glyphicon-map-marker map-marker-icon' "
                     + "aria-hidden='true' "
                     + "></span>"

            var row = '<tr>'
                    + '<td>' + button + '</td>'
                    + '<td>' + icon + stamp.name + '</td>'
                    + '<td>' + updated + '</td>'
                    + '</tr>';
            tbody.append(row);
            count++;
        });

        $('.imakoko-count').text(count);
    }

    /**
    * スタンプ情報をマーカー一覧に表示します。
    */
    function updateMarkerTable(stamps) {
        var tbody = $('#marker-table');
        tbody.find("tr:gt(0)").remove();
        var count = 0;
        stamps.forEach(function(stamp) {
            if (stamp.icon_type === 0) {
                return;
            }
            var updated = dateToString(new Date(stamp.updated));
            var button = "<button "
                       + "class=\"btn btn-primary\" "
                       + "data-dismiss=\"modal\" "
                       + "onClick=\"namespaceStampsheet.viewStampButtonClick("
                       +   "'" + stamp.id + "',"
                       +   "'" + stamp.latitude + "',"
                       +   "'" + stamp.longitude + "'"
                       + ");\""
                       + "><i class='glyphicon glyphicon-eye-open'></i></button>";

            var redIcon = "<img src='" + CONTEXT + "image/red-pushpin.png' />"
            var greenIcon = "<img src='" + CONTEXT + "image/grn-pushpin.png' />"
            var blueIcon = "<img src='" + CONTEXT + "image/blue-pushpin.png' />"
            var icon;
            if (stamp.icon_type === 1) {
                icon = redIcon;
            } else if (stamp.icon_type === 2) {
                icon = greenIcon;
            } else if (stamp.icon_type === 3) {
                icon = blueIcon;
            }

            var row = '<tr>'
                    + '<td>' + button + '</td>'
                    + '<td>' + icon + stamp.name + '</td>'
                    + '<td>' + updated + '</td>'
                    + '</tr>';
            tbody.append(row);
            count++;
        });
        
        $('.marker-count').text(count);
    }

    function dateToString(date) {
        var yyyy = date.getFullYear();
        var MM = date.getMonth() + 1;
        var dd = date.getDate();
        var hh = date.getHours();
        var mm = date.getMinutes();
        var ss = date.getSeconds();
        
        if (MM < 10) { MM = "0" + MM; }
        if (dd < 10) { dd = "0" + dd; }
        if (hh < 10) { hh = "0" + hh; }
        if (mm < 10) { mm = "0" + mm; }
        if (ss < 10) { ss = "0" + ss; }

        return yyyy + "/" + MM + "/" + dd
               + " " + hh + ":" + mm + ":" + ss;
    }

    function getMessage() {
        return $('#message').val();
    }

    function getStampSheetId() {
        return $('body').data('stampsheet-id');
    }

    function getMyName() {
        var myName = $.cookie("myName_" + getStampSheetId());
        if (myName) {
            return myName;
        } else {
            return "無名";
        }
    }

    function setMyName(value) {
        $.cookie("myName_" + getStampSheetId(), value, {expires: 7});
    }

    function getMyStampId(iconType) {
        return $.cookie("myStampId_" + getStampSheetId() + "_" + iconType);
    }

    function setMyStampId(value, iconType) {
        $.cookie("myStampId_" + getStampSheetId() + "_" + iconType, value, {expires: 7});
    }

    function getOpenStampId() {
        return $('body').data('open-stamp-id');
    }

    function shouldShowManualModal() {
        if($.cookie("invisibleManualModal_" + getStampSheetId())) {
            return false;
        } else {
            return true;
        }
    }

    function setInvisibleManualModal() {
        $.cookie("invisibleManualModal_" + getStampSheetId(), 'invisible', {expires: 100});
    }

    function setTraceMode(isTraceMode) {
        $("[name='trace']").bootstrapSwitch("state", isTraceMode);
    }

    /**
     * スタンプボタン押下時処理。
     */
    function _stampButtonClick() {
        var stampSheetId = getStampSheetId();
        var myName = getMyName();
        var message = getMessage();
        var position = map.fetchCurrentPosition();
        savePosition(
                stampSheetId,
                position.latitude,
                position.longitude,
                '【' + myName + '】' + message,
                0,
                function(stampId) {
                    updateStamp(stampSheetId, stampId);
                }
        );
    }

    /**
     * スタンプボタン自動押下時処理。
     */
    function _stampButtonAutoClick() {
        var stampSheetId = getStampSheetId();
        var myName = getMyName();
        var message = getMessage();
        var position = map.fetchCurrentPosition();
        savePosition(
                stampSheetId,
                position.latitude,
                position.longitude,
                '【' + myName + '】' + message,
                0,
                function(stampId) {
                    updateStamp(stampSheetId, null);
                }
        );
    }

    /**
     * マーカーボタン押下時処理。
     * @param {number} markerNo マーカーの番号
     */
    function _markerButtonClick(markerNo) {
        console.log("マーカーボタン押下[" + markerNo + "]");

        var stampSheetId = getStampSheetId();
        var myName = getMyName();
        var message = getMessage();
        var position = map.getCenter();
        savePosition(
                stampSheetId,
                position.latitude,
                position.longitude,
                '【' + myName + '】' + message,
                markerNo,
                function(stampId) {
                    updateStamp(stampSheetId, stampId);
                }
        );
    }

    return {
        
        stampButtonClick: _stampButtonClick,
        markerButtonClick: _markerButtonClick,

        latlngButtonClick:
            function() {
                map.loadCurrentPosition(navigator);
            },

        viewStampButtonClick:
            function(stampId, latitude, longitude) {
                setTraceMode(false);
                map.setCenter(latitude, longitude);
                map.openStampCommentWindow(stampId);
            },

        setCenter: function(latitude, longitude) {
            map.setCenter(latitude, longitude);
        },

        initialize: function() {
            var stampSheetId = getStampSheetId();
            var stampId = getOpenStampId();

            stampApi = new StampApi();
            map
                    = new Map(
                            $('#map-canvas')[0],
                            $('body').data('latitude'),
                            $('body').data('longitude'),
                            navigator,
                            function() {
                                if ($("input[name='autoRecord']").prop('checked')) {
                                    console.log("自動記録");
                                    _stampButtonAutoClick();
                                }
                                $('#disabledLocationLabel').addClass('hidden');
                            },
                            function() {
                                $('#disabledLocationLabel').removeClass('hidden');
                            });

            $(function() {
                updateStamp(stampSheetId, stampId);
                setInterval(function() {
                    updateStamp(stampSheetId);
                }, 60000);
            });

            // 名前
            $("#nameTextBox").change(function() {
                $("#name-label").text($(this).val());
                setMyName($(this).val());
            });
            $("#name-label").text(getMyName());
            $("#nameTextBox").val(getMyName());
            
            // 送信ボタン
            $("#stampButton").click(function() {
                namespaceStampsheet.stampButtonClick();
            });

            // マーカーの送信ボタン
            $(".dropdown-menu a").click(function() {
                namespaceStampsheet.markerButtonClick($(this).attr('value'));
            });

            // URLテキストボックス
            $("#urlTextBox").val(location.href);
            
            // 自分追跡設定
            if($('body').data('trace-mode') === 'trace') {
                $("[name='trace']").prop("checked", true);
            } else {
                $("[name='trace']").prop("checked", false);
            }
            $("[name='trace']").bootstrapSwitch();

            $('#manualModal').on('hide.bs.modal', function(event) {
                if ($("input[name='invisibleManualModal']").prop('checked')) {
                    setInvisibleManualModal();
                }
                $('#nameModal').modal('show');
            });

            if(shouldShowManualModal()) {
                $('#manualModal').modal('show');
            }

            // 自動記録
            if($('body').data('record-mode') === 'auto') {
                $("input[name='autoRecord']").prop("checked", true);
            } else {
                $("input[name='autoRecord']").prop("checked", false);
            }
            $("input[name='autoRecord']").bootstrapSwitch();
            $("input[name='autoRecord']").on('switchChange.bootstrapSwitch', function(event, state) {
                if (state) {
                    $("#stampButton").prop('disabled', true);
                    $("#autoRecordLabel").removeClass('hidden');
                } else {
                    $("#stampButton").prop('disabled', false);
                    $("#autoRecordLabel").addClass('hidden');
                }
            });
        }

    };
}(navigator));

namespaceStampsheet.initialize();
