package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.BsStampSheetsBhv;

/**
 * The behavior of stamp_sheets.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("stampSheetsBhv")
public class StampSheetsBhv extends BsStampSheetsBhv {
}
