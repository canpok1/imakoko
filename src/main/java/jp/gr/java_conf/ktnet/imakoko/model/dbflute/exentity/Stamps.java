package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.BsStamps;

/**
 * The entity of stamps.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Stamps extends BsStamps {

    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;
}
