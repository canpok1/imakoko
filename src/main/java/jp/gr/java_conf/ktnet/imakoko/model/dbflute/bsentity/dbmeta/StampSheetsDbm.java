package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The DB meta of stamp_sheets. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class StampSheetsDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final StampSheetsDbm _instance = new StampSheetsDbm();
    private StampSheetsDbm() {}
    public static StampSheetsDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((StampSheets)et).getId(), (et, vl) -> ((StampSheets)et).setId((String)vl), "id");
        setupEpg(_epgMap, et -> ((StampSheets)et).getCreated(), (et, vl) -> ((StampSheets)et).setCreated(ctldt(vl)), "created");
        setupEpg(_epgMap, et -> ((StampSheets)et).getUpdated(), (et, vl) -> ((StampSheets)et).setUpdated(ctldt(vl)), "updated");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "stamp_sheets";
    protected final String _tableDispName = "stamp_sheets";
    protected final String _tablePropertyName = "stampSheets";
    protected final TableSqlName _tableSqlName = new TableSqlName("stamp_sheets", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnId = cci("id", "id", null, null, String.class, "id", null, true, false, true, "varchar", 36, 0, null, false, null, null, null, "stampsList", null, false);
    protected final ColumnInfo _columnCreated = cci("created", "created", null, null, java.time.LocalDateTime.class, "created", null, false, false, true, "timestamp", 29, 6, "now()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUpdated = cci("updated", "updated", null, null, java.time.LocalDateTime.class, "updated", null, false, false, true, "timestamp", 29, 6, "now()", false, null, null, null, null, null, false);

    /**
     * id: {PK, NotNull, varchar(36)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnId() { return _columnId; }
    /**
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCreated() { return _columnCreated; }
    /**
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUpdated() { return _columnUpdated; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnId());
        ls.add(columnCreated());
        ls.add(columnUpdated());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------
    /**
     * stamps by stamp_sheet_id, named 'stampsList'.
     * @return The information object of referrer property. (NotNull)
     */
    public ReferrerInfo referrerStampsList() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnId(), StampsDbm.getInstance().columnStampSheetId());
        return cri("stamps_stamp_sheet_id_fkey", "stampsList", this, StampsDbm.getInstance(), mp, false, "stampSheets");
    }

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.StampSheets"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.StampSheetsCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.StampSheetsBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<StampSheets> getEntityType() { return StampSheets.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public StampSheets newEntity() { return new StampSheets(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((StampSheets)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((StampSheets)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
