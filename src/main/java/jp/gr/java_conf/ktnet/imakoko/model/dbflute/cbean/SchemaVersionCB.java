package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.bs.BsSchemaVersionCB;

/**
 * The condition-bean of schema_version.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaVersionCB extends BsSchemaVersionCB {
}
