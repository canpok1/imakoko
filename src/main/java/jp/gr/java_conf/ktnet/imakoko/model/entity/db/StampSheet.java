package jp.gr.java_conf.ktnet.imakoko.model.entity.db;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.StampSheets;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class StampSheet {
  private String id;
  private Date created;
  private Date updated;
  
  /**
   * エンティティを元にインスタンスを生成します.
   * @param entity エンティティ.
   */
  public StampSheet(@NonNull final StampSheets entity) {
    this(
        entity.getId(),
        null,
        null
    );
  }
}
