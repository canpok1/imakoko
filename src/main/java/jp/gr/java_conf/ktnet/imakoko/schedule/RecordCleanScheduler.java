package jp.gr.java_conf.ktnet.imakoko.schedule;

import jp.gr.java_conf.ktnet.imakoko.model.StampService;
import jp.gr.java_conf.ktnet.imakoko.model.StampSheetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * レコードを定期的に削除するクラスです.
 * @author tanabe
 *
 */
@Component
@Slf4j
public class RecordCleanScheduler {

  @Autowired
  private StampService stampService;
  
  @Autowired
  private StampSheetService stampSheetService;
  
  /**
   * スタンプの有効日数.
   */
  private static final int ACTIVE_DATE_COUNT = 2;
  
  /**
   * 無効なスタンプとシートを削除します.
   */
  @Scheduled(initialDelay = 10000, fixedDelay = 86400000)
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void clean() {
    log.info("[定期実行]{}日間未更新のシートを削除 開始", ACTIVE_DATE_COUNT);
    stampSheetService.deleteInactiveStamp(ACTIVE_DATE_COUNT);
    log.info("[定期実行]終了");
  }
}
