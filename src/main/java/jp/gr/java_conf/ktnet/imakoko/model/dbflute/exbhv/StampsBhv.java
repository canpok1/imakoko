package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.BsStampsBhv;

/**
 * The behavior of stamps.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("stampsBhv")
public class StampsBhv extends BsStampsBhv {
}
