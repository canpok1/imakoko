package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The DB meta of schema_version. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class SchemaVersionDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final SchemaVersionDbm _instance = new SchemaVersionDbm();
    private SchemaVersionDbm() {}
    public static SchemaVersionDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getInstalledRank(), (et, vl) -> ((SchemaVersion)et).setInstalledRank(cti(vl)), "installedRank");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getVersion(), (et, vl) -> ((SchemaVersion)et).setVersion((String)vl), "version");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getDescription(), (et, vl) -> ((SchemaVersion)et).setDescription((String)vl), "description");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getType(), (et, vl) -> ((SchemaVersion)et).setType((String)vl), "type");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getScript(), (et, vl) -> ((SchemaVersion)et).setScript((String)vl), "script");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getChecksum(), (et, vl) -> ((SchemaVersion)et).setChecksum(cti(vl)), "checksum");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getInstalledBy(), (et, vl) -> ((SchemaVersion)et).setInstalledBy((String)vl), "installedBy");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getInstalledOn(), (et, vl) -> ((SchemaVersion)et).setInstalledOn(ctldt(vl)), "installedOn");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getExecutionTime(), (et, vl) -> ((SchemaVersion)et).setExecutionTime(cti(vl)), "executionTime");
        setupEpg(_epgMap, et -> ((SchemaVersion)et).getSuccess(), (et, vl) -> ((SchemaVersion)et).setSuccess((Boolean)vl), "success");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "schema_version";
    protected final String _tableDispName = "schema_version";
    protected final String _tablePropertyName = "schemaVersion";
    protected final TableSqlName _tableSqlName = new TableSqlName("schema_version", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnInstalledRank = cci("installed_rank", "installed_rank", null, null, Integer.class, "installedRank", null, true, false, true, "int4", 10, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnVersion = cci("version", "version", null, null, String.class, "version", null, false, false, false, "varchar", 50, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnDescription = cci("description", "description", null, null, String.class, "description", null, false, false, true, "varchar", 200, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnType = cci("type", "type", null, null, String.class, "type", null, false, false, true, "varchar", 20, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnScript = cci("script", "script", null, null, String.class, "script", null, false, false, true, "varchar", 1000, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnChecksum = cci("checksum", "checksum", null, null, Integer.class, "checksum", null, false, false, false, "int4", 10, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnInstalledBy = cci("installed_by", "installed_by", null, null, String.class, "installedBy", null, false, false, true, "varchar", 100, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnInstalledOn = cci("installed_on", "installed_on", null, null, java.time.LocalDateTime.class, "installedOn", null, false, false, true, "timestamp", 29, 6, "now()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnExecutionTime = cci("execution_time", "execution_time", null, null, Integer.class, "executionTime", null, false, false, true, "int4", 10, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnSuccess = cci("success", "success", null, null, Boolean.class, "success", null, false, false, true, "bool", 1, 0, null, false, null, null, null, null, null, false);

    /**
     * installed_rank: {PK, NotNull, int4(10)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnInstalledRank() { return _columnInstalledRank; }
    /**
     * version: {varchar(50)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnVersion() { return _columnVersion; }
    /**
     * description: {NotNull, varchar(200)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnDescription() { return _columnDescription; }
    /**
     * type: {NotNull, varchar(20)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnType() { return _columnType; }
    /**
     * script: {NotNull, varchar(1000)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnScript() { return _columnScript; }
    /**
     * checksum: {int4(10)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnChecksum() { return _columnChecksum; }
    /**
     * installed_by: {NotNull, varchar(100)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnInstalledBy() { return _columnInstalledBy; }
    /**
     * installed_on: {NotNull, timestamp(29, 6), default=[now()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnInstalledOn() { return _columnInstalledOn; }
    /**
     * execution_time: {NotNull, int4(10)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnExecutionTime() { return _columnExecutionTime; }
    /**
     * success: {IX, NotNull, bool(1)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnSuccess() { return _columnSuccess; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnInstalledRank());
        ls.add(columnVersion());
        ls.add(columnDescription());
        ls.add(columnType());
        ls.add(columnScript());
        ls.add(columnChecksum());
        ls.add(columnInstalledBy());
        ls.add(columnInstalledOn());
        ls.add(columnExecutionTime());
        ls.add(columnSuccess());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnInstalledRank()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.SchemaVersion"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.SchemaVersionCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.SchemaVersionBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<SchemaVersion> getEntityType() { return SchemaVersion.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public SchemaVersion newEntity() { return new SchemaVersion(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((SchemaVersion)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((SchemaVersion)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
