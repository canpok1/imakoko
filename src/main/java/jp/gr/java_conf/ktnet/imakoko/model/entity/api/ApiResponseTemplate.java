package jp.gr.java_conf.ktnet.imakoko.model.entity.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * API用のレスポンス向けクラスです.
 * @author tanabe
 *
 * @param <T> レスポンスに含める情報の型.
 */
@Data
@RequiredArgsConstructor
public class ApiResponseTemplate<T> {
  @ApiModelProperty(value = "メッセージ", required = true)
  private final String message;
  @ApiModelProperty(value = "詳細", required = false)
  private final T body;
}
