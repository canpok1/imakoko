package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.BsStampSheets;

/**
 * The entity of stamp_sheets.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class StampSheets extends BsStampSheets {

    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;
}
