/**
 * 定期的に実行する処理を記載したクラス向けパッケージです.
 * @author tanabe
 */
package jp.gr.java_conf.ktnet.imakoko.schedule;