package jp.gr.java_conf.ktnet.imakoko.model.repository.impl;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.StampsBhv;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.Stamps;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampRepository;
import jp.gr.java_conf.ktnet.imakoko.model.repository.mapper.StampMapper;
import lombok.NonNull;

import org.dbflute.exception.EntityAlreadyDeletedException;
import org.dbflute.optional.OptionalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * stampsテーブルの情報を操作するクラスです.
 * @author tanabe
 */
@Repository
public class StampRepositoryImpl implements StampRepository {

  @Autowired
  private StampMapper mapper;
  
  @Autowired
  private StampsBhv stampBhv;
  
  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository#findAll()
   */
  @Override
  public List<Stamp> findAll() {
    return stampBhv
            .selectList(cb -> {
                cb.query().addOrderBy_Id_Desc();
              })
            .mappingList(entity -> new Stamp(entity));
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository
   * #findById(java.lang.String, java.lang.String)
   */
  @Override
  public Stamp findById(
      final String id,
      final String stampSheetId) {
    return mapper.findById(id, stampSheetId);
//    OptionalEntity<Stamp> stamp = stampBhv
//        .selectEntity(cb -> {
//          cb.query().setId_Equal(id);
//          cb.query().setStampSheetId_Equal(stampSheetId);
//        })
//        .map(entity -> {
//          return new Stamp(entity);
//        });
//    if (stamp.isPresent()) {
//      return stamp.get();
//    } else {
//      return null;
//    }
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository
   * #insert(jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp)
   */
  @Override
  public String insert(@NonNull final Stamp record) {
    UUID uuid = UUID.randomUUID();
    
    Stamp newRecord = new Stamp(record);
    newRecord.setId(uuid.toString());
    mapper.insert(newRecord);
    
//    Stamps newRecord = new Stamps();
//    newRecord.setId(uuid.toString());
//    newRecord.setStampSheetId(record.getStampSheetId());
//    newRecord.setName(record.getName());
//    newRecord.setLatitude(record.getLatitude());
//    newRecord.setLongitude(record.getLongitude());
//    newRecord.setIconType(record.getIconType());
//    newRecord.setCreated(LocalDateTime.now());
//    newRecord.setUpdated(LocalDateTime.now());
//    stampBhv.insert(newRecord);
    
    return newRecord.getId();
  }
  
  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository
   * #update(jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp)
   */
  @Override
  public String update(@NonNull final Stamp record) {
    Stamps stamp = new Stamps();
    stamp.setId(record.getId());
    stamp.setStampSheetId(record.getStampSheetId());
    stamp.setName(record.getName());
    stamp.setLatitude(record.getLatitude());
    stamp.setLongitude(record.getLongitude());
    stamp.setIconType(record.getIconType());
    stamp.setUpdated(LocalDateTime.now());
    
    try {
      stampBhv.update(stamp);
      return record.getId();
    } catch (EntityAlreadyDeletedException ex) {
      return null;
    }
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository
   * #findByStampSheetId(java.lang.String)
   */
  @Override
  public List<Stamp> findByStampSheetId(String stampSheetId) {
    return stampBhv
              .selectList(cb -> {
                  cb.query().setStampSheetId_Equal(stampSheetId);
                  cb.query().addOrderBy_Id_Desc();
                })
              .mappingList(entity -> new Stamp(entity));
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampRepository
   * #deleteByStampSheetId(java.util.List)
   */
  @Override
  public void deleteByStampSheetId(List<String> stampSheetId) {
    if (stampSheetId != null && !stampSheetId.isEmpty()) {
      mapper.deleteByStampSheetId(stampSheetId);
    }
  }
  
}
