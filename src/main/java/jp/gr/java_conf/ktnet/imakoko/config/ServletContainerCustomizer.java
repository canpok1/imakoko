package jp.gr.java_conf.ktnet.imakoko.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * エラーページなどの設定を行うクラスです.
 * @author tanabe
 *
 */
@Component
public class ServletContainerCustomizer implements EmbeddedServletContainerCustomizer {

  /**
   * 設定を行います.
   * @param container コンテナ.
   */
  @Override
  public void customize(ConfigurableEmbeddedServletContainer container) {
    container.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/404"));
    container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/404"));
    container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500"));
    container.addErrorPages(new ErrorPage(HttpStatus.SERVICE_UNAVAILABLE, "/503"));
  }

}
