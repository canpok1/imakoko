package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.ciq;

import java.util.Map;
import org.dbflute.cbean.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.ConditionOption;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.bs.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.*;

/**
 * The condition-query for in-line of schema_version.
 * @author DBFlute(AutoGenerator)
 */
public class SchemaVersionCIQ extends AbstractBsSchemaVersionCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected BsSchemaVersionCQ _myCQ;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public SchemaVersionCIQ(ConditionQuery referrerQuery, SqlClause sqlClause
                        , String aliasName, int nestLevel, BsSchemaVersionCQ myCQ) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
        _myCQ = myCQ;
        _foreignPropertyName = _myCQ.xgetForeignPropertyName(); // accept foreign property name
        _relationPath = _myCQ.xgetRelationPath(); // accept relation path
        _inline = true;
    }

    // ===================================================================================
    //                                                             Override about Register
    //                                                             =======================
    protected void reflectRelationOnUnionQuery(ConditionQuery bq, ConditionQuery uq)
    { throw new IllegalConditionBeanOperationException("InlineView cannot use Union: " + bq + " : " + uq); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col)
    { regIQ(k, v, cv, col); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col, ConditionOption op)
    { regIQ(k, v, cv, col, op); }

    @Override
    protected void registerWhereClause(String wc)
    { registerInlineWhereClause(wc); }

    @Override
    protected boolean isInScopeRelationSuppressLocalAliasName() {
        if (_onClause) { throw new IllegalConditionBeanOperationException("InScopeRelation on OnClause is unsupported."); }
        return true;
    }

    // ===================================================================================
    //                                                                Override about Query
    //                                                                ====================
    protected ConditionValue xgetCValueInstalledRank() { return _myCQ.xdfgetInstalledRank(); }
    protected ConditionValue xgetCValueVersion() { return _myCQ.xdfgetVersion(); }
    protected ConditionValue xgetCValueDescription() { return _myCQ.xdfgetDescription(); }
    protected ConditionValue xgetCValueType() { return _myCQ.xdfgetType(); }
    protected ConditionValue xgetCValueScript() { return _myCQ.xdfgetScript(); }
    protected ConditionValue xgetCValueChecksum() { return _myCQ.xdfgetChecksum(); }
    protected ConditionValue xgetCValueInstalledBy() { return _myCQ.xdfgetInstalledBy(); }
    protected ConditionValue xgetCValueInstalledOn() { return _myCQ.xdfgetInstalledOn(); }
    protected ConditionValue xgetCValueExecutionTime() { return _myCQ.xdfgetExecutionTime(); }
    protected ConditionValue xgetCValueSuccess() { return _myCQ.xdfgetSuccess(); }
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String pp) { return null; }
    public String keepScalarCondition(SchemaVersionCQ sq)
    { throwIICBOE("ScalarCondition"); return null; }
    public String keepSpecifyMyselfDerived(SchemaVersionCQ sq)
    { throwIICBOE("(Specify)MyselfDerived"); return null;}
    public String keepQueryMyselfDerived(SchemaVersionCQ sq)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepQueryMyselfDerivedParameter(Object vl)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepMyselfExists(SchemaVersionCQ sq)
    { throwIICBOE("MyselfExists"); return null;}

    protected void throwIICBOE(String name)
    { throw new IllegalConditionBeanOperationException(name + " at InlineView is unsupported."); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xinCB() { return SchemaVersionCB.class.getName(); }
    protected String xinCQ() { return SchemaVersionCQ.class.getName(); }
}
