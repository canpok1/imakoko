package jp.gr.java_conf.ktnet.imakoko.model.repository;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;

import java.util.List;

/**
 * stampsテーブルの情報を操作するクラスです.
 * @author tanabe
 */
public interface StampRepository {

  /**
   * 全レコードを取得します.
   * @return 全レコード.見つからない時は空のList.
   */
  List<Stamp> findAll();

  /**
   * 指定IDのレコードを取得します.
   * @param id ID.
   * @param stampSheetId シートID.
   * @return レコード.見つからない時はnull.
   */
  Stamp findById(String id, String stampSheetId);

  /**
   * 指定のレコードを新規作成します.
   * @param record レコード.
   * @return 割り当てられたID.
   */
  String insert(Stamp record);

  /**
   * 指定のレコードを更新します.
   * @param record レコード.
   * @return 更新したレコードのID.
   */
  String update(Stamp record);

  /**
   * 指定スタンプシートIDのレコードを取得します.
   * @param stampSheetId スタンプシートID.
   * @return レコード. 見つからない場合は空.
   */
  List<Stamp> findByStampSheetId(String stampSheetId);

  /**
   * 指定スタンプシートIDのレコードを削除します.
   * @param stampSheetId スタンプシートID.
   */
  void deleteByStampSheetId(List<String> stampSheetId);

}