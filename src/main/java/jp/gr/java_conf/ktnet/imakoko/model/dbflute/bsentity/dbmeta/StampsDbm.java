package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.optional.OptionalEntity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The DB meta of stamps. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class StampsDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final StampsDbm _instance = new StampsDbm();
    private StampsDbm() {}
    public static StampsDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((Stamps)et).getId(), (et, vl) -> ((Stamps)et).setId((String)vl), "id");
        setupEpg(_epgMap, et -> ((Stamps)et).getStampSheetId(), (et, vl) -> ((Stamps)et).setStampSheetId((String)vl), "stampSheetId");
        setupEpg(_epgMap, et -> ((Stamps)et).getName(), (et, vl) -> ((Stamps)et).setName((String)vl), "name");
        setupEpg(_epgMap, et -> ((Stamps)et).getLatitude(), (et, vl) -> ((Stamps)et).setLatitude((String)vl), "latitude");
        setupEpg(_epgMap, et -> ((Stamps)et).getLongitude(), (et, vl) -> ((Stamps)et).setLongitude((String)vl), "longitude");
        setupEpg(_epgMap, et -> ((Stamps)et).getCreated(), (et, vl) -> ((Stamps)et).setCreated(ctldt(vl)), "created");
        setupEpg(_epgMap, et -> ((Stamps)et).getUpdated(), (et, vl) -> ((Stamps)et).setUpdated(ctldt(vl)), "updated");
        setupEpg(_epgMap, et -> ((Stamps)et).getIconType(), (et, vl) -> ((Stamps)et).setIconType(cti(vl)), "iconType");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    protected final Map<String, PropertyGateway> _efpgMap = newHashMap();
    { xsetupEfpg(); }
    @SuppressWarnings("unchecked")
    protected void xsetupEfpg() {
        setupEfpg(_efpgMap, et -> ((Stamps)et).getStampSheets(), (et, vl) -> ((Stamps)et).setStampSheets((OptionalEntity<StampSheets>)vl), "stampSheets");
    }
    public PropertyGateway findForeignPropertyGateway(String prop)
    { return doFindEfpg(_efpgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "stamps";
    protected final String _tableDispName = "stamps";
    protected final String _tablePropertyName = "stamps";
    protected final TableSqlName _tableSqlName = new TableSqlName("stamps", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnId = cci("id", "id", null, null, String.class, "id", null, true, false, true, "varchar", 36, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnStampSheetId = cci("stamp_sheet_id", "stamp_sheet_id", null, null, String.class, "stampSheetId", null, false, false, true, "varchar", 36, 0, null, false, null, null, "stampSheets", null, null, false);
    protected final ColumnInfo _columnName = cci("name", "name", null, null, String.class, "name", null, false, false, true, "varchar", 64, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnLatitude = cci("latitude", "latitude", null, null, String.class, "latitude", null, false, false, true, "text", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnLongitude = cci("longitude", "longitude", null, null, String.class, "longitude", null, false, false, true, "text", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnCreated = cci("created", "created", null, null, java.time.LocalDateTime.class, "created", null, false, false, true, "timestamp", 29, 6, "now()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUpdated = cci("updated", "updated", null, null, java.time.LocalDateTime.class, "updated", null, false, false, true, "timestamp", 29, 6, "now()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnIconType = cci("icon_type", "icon_type", null, null, Integer.class, "iconType", null, false, false, false, "int4", 10, 0, "0", false, null, null, null, null, null, false);

    /**
     * id: {PK, NotNull, varchar(36)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnId() { return _columnId; }
    /**
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnStampSheetId() { return _columnStampSheetId; }
    /**
     * name: {NotNull, varchar(64)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnName() { return _columnName; }
    /**
     * latitude: {NotNull, text(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnLatitude() { return _columnLatitude; }
    /**
     * longitude: {NotNull, text(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnLongitude() { return _columnLongitude; }
    /**
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCreated() { return _columnCreated; }
    /**
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUpdated() { return _columnUpdated; }
    /**
     * icon_type: {int4(10), default=[0]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnIconType() { return _columnIconType; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnId());
        ls.add(columnStampSheetId());
        ls.add(columnName());
        ls.add(columnLatitude());
        ls.add(columnLongitude());
        ls.add(columnCreated());
        ls.add(columnUpdated());
        ls.add(columnIconType());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    /**
     * stamp_sheets by my stamp_sheet_id, named 'stampSheets'.
     * @return The information object of foreign property. (NotNull)
     */
    public ForeignInfo foreignStampSheets() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnStampSheetId(), StampSheetsDbm.getInstance().columnId());
        return cfi("stamps_stamp_sheet_id_fkey", "stampSheets", this, StampSheetsDbm.getInstance(), mp, 0, org.dbflute.optional.OptionalEntity.class, false, false, false, false, null, null, false, "stampsList", false);
    }

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.Stamps"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.StampsCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.StampsBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<Stamps> getEntityType() { return Stamps.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public Stamps newEntity() { return new Stamps(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((Stamps)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((Stamps)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
