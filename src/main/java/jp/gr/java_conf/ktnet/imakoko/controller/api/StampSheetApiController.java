package jp.gr.java_conf.ktnet.imakoko.controller.api;

import io.swagger.annotations.ApiModelProperty;
import jp.gr.java_conf.ktnet.imakoko.model.StampSheetService;
import jp.gr.java_conf.ktnet.imakoko.model.entity.api.ApiResponseTemplate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * スタンプシートAPIを定義するコントローラーです.
 * @author tanabe
 */
@RestController
@RequestMapping("/api/stampsheets")
@Slf4j
public class StampSheetApiController {
  
  @Autowired
  private StampSheetService stampSheetService;
  
  @Data
  @AllArgsConstructor
  public static class MakeStampSheetResponse {
    @ApiModelProperty(value = "割り当てられたID", required = true)
    private final String id;
  }
  
  /**
   * スタンプシートを新しく生成します.
   * @param response レスポンス.
   * @return レスポンスのボディ.
   * @throws Exception 例外発生.
   */
  @RequestMapping(method = RequestMethod.POST)
  public ApiResponseTemplate<MakeStampSheetResponse> makeStampSheet(
      final HttpServletResponse response
  ) throws Exception {
    
    String id = stampSheetService.create();
    
    MakeStampSheetResponse responseBody = new MakeStampSheetResponse(id);
    return new ApiResponseTemplate<MakeStampSheetResponse>("OK", responseBody);
  }
}
