package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The referrer loader of stamps as TABLE. <br>
 * <pre>
 * [primary key]
 *     id
 *
 * [column]
 *     id, stamp_sheet_id, name, latitude, longitude, created, updated, icon_type
 *
 * [sequence]
 *     
 *
 * [identity]
 *     
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     stamp_sheets
 *
 * [referrer table]
 *     
 *
 * [foreign property]
 *     stampSheets
 *
 * [referrer property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfStamps {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<Stamps> _selectedList;
    protected BehaviorSelector _selector;
    protected StampsBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfStamps ready(List<Stamps> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected StampsBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(StampsBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    protected LoaderOfStampSheets _foreignStampSheetsLoader;
    public LoaderOfStampSheets pulloutStampSheets() {
        if (_foreignStampSheetsLoader == null)
        { _foreignStampSheetsLoader = new LoaderOfStampSheets().ready(myBhv().pulloutStampSheets(_selectedList), _selector); }
        return _foreignStampSheetsLoader;
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<Stamps> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
