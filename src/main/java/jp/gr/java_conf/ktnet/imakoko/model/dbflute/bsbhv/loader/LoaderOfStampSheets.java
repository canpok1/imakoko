package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import org.dbflute.bhv.referrer.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.*;

/**
 * The referrer loader of stamp_sheets as TABLE. <br>
 * <pre>
 * [primary key]
 *     id
 *
 * [column]
 *     id, created, updated
 *
 * [sequence]
 *     
 *
 * [identity]
 *     
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     
 *
 * [referrer table]
 *     stamps
 *
 * [foreign property]
 *     
 *
 * [referrer property]
 *     stampsList
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfStampSheets {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<StampSheets> _selectedList;
    protected BehaviorSelector _selector;
    protected StampSheetsBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfStampSheets ready(List<StampSheets> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected StampSheetsBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(StampSheetsBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                       Load Referrer
    //                                                                       =============
    protected List<Stamps> _referrerStamps;

    /**
     * Load referrer of stampsList by the set-upper of referrer. <br>
     * stamps by stamp_sheet_id, named 'stampsList'.
     * <pre>
     * <span style="color: #0000C0">stampSheetsBhv</span>.<span style="color: #994747">load</span>(<span style="color: #553000">stampSheetsList</span>, <span style="color: #553000">sheetsLoader</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">sheetsLoader</span>.<span style="color: #CC4747">loadStamps</span>(<span style="color: #553000">stampsCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *         <span style="color: #553000">stampsCB</span>.setupSelect...
     *         <span style="color: #553000">stampsCB</span>.query().set...
     *         <span style="color: #553000">stampsCB</span>.query().addOrderBy...
     *     }); <span style="color: #3F7E5E">// you can load nested referrer from here</span>
     *     <span style="color: #3F7E5E">//}).withNestedReferrer(<span style="color: #553000">stampsLoader</span> -&gt; {</span>
     *     <span style="color: #3F7E5E">//    stampsLoader.load...</span>
     *     <span style="color: #3F7E5E">//});</span>
     * });
     * for (StampSheets stampSheets : <span style="color: #553000">stampSheetsList</span>) {
     *     ... = stampSheets.<span style="color: #CC4747">getStampsList()</span>;
     * }
     * </pre>
     * About internal policy, the value of primary key (and others too) is treated as case-insensitive. <br>
     * The condition-bean, which the set-upper provides, has settings before callback as follows:
     * <pre>
     * cb.query().setStampSheetId_InScope(pkList);
     * cb.query().addOrderBy_StampSheetId_Asc();
     * </pre>
     * @param refCBLambda The callback to set up referrer condition-bean for loading referrer. (NotNull)
     * @return The callback interface which you can load nested referrer by calling withNestedReferrer(). (NotNull)
     */
    public NestedReferrerLoaderGateway<LoaderOfStamps> loadStamps(ReferrerConditionSetupper<StampsCB> refCBLambda) {
        myBhv().loadStamps(_selectedList, refCBLambda).withNestedReferrer(refLs -> _referrerStamps = refLs);
        return hd -> hd.handle(new LoaderOfStamps().ready(_referrerStamps, _selector));
    }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<StampSheets> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
