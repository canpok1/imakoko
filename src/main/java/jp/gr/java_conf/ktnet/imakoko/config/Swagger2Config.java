package jp.gr.java_conf.ktnet.imakoko.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {

  /**
   * プラグインの設定オブジェクトを返します.
   * @return 設定オブジェクト.
   */
  @Bean
  public Docket swaggerSpringMvcPlugin() {
    return new Docket(DocumentationType.SWAGGER_2)
                        .select()
                        .paths(paths())
                        .build()
                        .apiInfo(apiInfo());
  }

  /**
   * 仕様書生成対象のURLパスを生成します.
   * @return 仕様書生成対象のURLパス
   */
  private Predicate<String> paths() {
    return Predicates
              .and(Predicates.containsPattern("/api/*"));
  }

  /**
   * 仕様書の情報を生成します.
   * @return 仕様書の情報.
   */
  private ApiInfo apiInfo() {
    ApiInfo apiInfo = new ApiInfo(
                "Imakoko Web API", // title
                "いまここマップ の Web API 仕様書", // description
                "0.0.1", // version
                "", // terms of service url
                "", // created by
                "", // license
                "" // license url
                );
    return apiInfo;
  }
}
