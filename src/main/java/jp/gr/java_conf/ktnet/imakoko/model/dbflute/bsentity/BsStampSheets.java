package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity;

import java.util.List;
import java.util.ArrayList;

import org.dbflute.dbmeta.DBMeta;
import org.dbflute.dbmeta.AbstractEntity;
import org.dbflute.dbmeta.accessory.DomainEntity;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.DBMetaInstanceHandler;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The entity of stamp_sheets as TABLE. <br>
 * スタンプシートテーブル
 * <pre>
 * [primary-key]
 *     id
 *
 * [column]
 *     id, created, updated
 *
 * [sequence]
 *     
 *
 * [identity]
 *     
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     
 *
 * [referrer table]
 *     stamps
 *
 * [foreign property]
 *     
 *
 * [referrer property]
 *     stampsList
 *
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * String id = entity.getId();
 * java.time.LocalDateTime created = entity.getCreated();
 * java.time.LocalDateTime updated = entity.getUpdated();
 * entity.setId(id);
 * entity.setCreated(created);
 * entity.setUpdated(updated);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsStampSheets extends AbstractEntity implements DomainEntity {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    /** id: {PK, NotNull, varchar(36)} */
    protected String _id;

    /** created: {NotNull, timestamp(29, 6), default=[now()]} */
    protected java.time.LocalDateTime _created;

    /** updated: {NotNull, timestamp(29, 6), default=[now()]} */
    protected java.time.LocalDateTime _updated;

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    /** {@inheritDoc} */
    public DBMeta asDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(asTableDbName());
    }

    /** {@inheritDoc} */
    public String asTableDbName() {
        return "stamp_sheets";
    }

    // ===================================================================================
    //                                                                        Key Handling
    //                                                                        ============
    /** {@inheritDoc} */
    public boolean hasPrimaryKeyValue() {
        if (_id == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    /** stamps by stamp_sheet_id, named 'stampsList'. */
    protected List<Stamps> _stampsList;

    /**
     * [get] stamps by stamp_sheet_id, named 'stampsList'.
     * @return The entity list of referrer property 'stampsList'. (NotNull: even if no loading, returns empty list)
     */
    public List<Stamps> getStampsList() {
        if (_stampsList == null) { _stampsList = newReferrerList(); }
        return _stampsList;
    }

    /**
     * [set] stamps by stamp_sheet_id, named 'stampsList'.
     * @param stampsList The entity list of referrer property 'stampsList'. (NullAllowed)
     */
    public void setStampsList(List<Stamps> stampsList) {
        _stampsList = stampsList;
    }

    protected <ELEMENT> List<ELEMENT> newReferrerList() { // overriding to import
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    @Override
    protected boolean doEquals(Object obj) {
        if (obj instanceof BsStampSheets) {
            BsStampSheets other = (BsStampSheets)obj;
            if (!xSV(_id, other._id)) { return false; }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected int doHashCode(int initial) {
        int hs = initial;
        hs = xCH(hs, asTableDbName());
        hs = xCH(hs, _id);
        return hs;
    }

    @Override
    protected String doBuildStringWithRelation(String li) {
        StringBuilder sb = new StringBuilder();
        if (_stampsList != null) { for (Stamps et : _stampsList)
        { if (et != null) { sb.append(li).append(xbRDS(et, "stampsList")); } } }
        return sb.toString();
    }

    @Override
    protected String doBuildColumnString(String dm) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm).append(xfND(_id));
        sb.append(dm).append(xfND(_created));
        sb.append(dm).append(xfND(_updated));
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    @Override
    protected String doBuildRelationString(String dm) {
        StringBuilder sb = new StringBuilder();
        if (_stampsList != null && !_stampsList.isEmpty())
        { sb.append(dm).append("stampsList"); }
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length()).insert(0, "(").append(")");
        }
        return sb.toString();
    }

    @Override
    public StampSheets clone() {
        return (StampSheets)super.clone();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] id: {PK, NotNull, varchar(36)} <br>
     * ID
     * @return The value of the column 'id'. (basically NotNull if selected: for the constraint)
     */
    public String getId() {
        checkSpecifiedProperty("id");
        return _id;
    }

    /**
     * [set] id: {PK, NotNull, varchar(36)} <br>
     * ID
     * @param id The value of the column 'id'. (basically NotNull if update: for the constraint)
     */
    public void setId(String id) {
        registerModifiedProperty("id");
        _id = id;
    }

    /**
     * [get] created: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 作成日時
     * @return The value of the column 'created'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getCreated() {
        checkSpecifiedProperty("created");
        return _created;
    }

    /**
     * [set] created: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 作成日時
     * @param created The value of the column 'created'. (basically NotNull if update: for the constraint)
     */
    public void setCreated(java.time.LocalDateTime created) {
        registerModifiedProperty("created");
        _created = created;
    }

    /**
     * [get] updated: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 更新日時
     * @return The value of the column 'updated'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getUpdated() {
        checkSpecifiedProperty("updated");
        return _updated;
    }

    /**
     * [set] updated: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 更新日時
     * @param updated The value of the column 'updated'. (basically NotNull if update: for the constraint)
     */
    public void setUpdated(java.time.LocalDateTime updated) {
        registerModifiedProperty("updated");
        _updated = updated;
    }
}
