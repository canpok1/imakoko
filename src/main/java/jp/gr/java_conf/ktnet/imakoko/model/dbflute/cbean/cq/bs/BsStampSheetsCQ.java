package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.bs;

import java.util.Map;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.ciq.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.*;

/**
 * The base condition-query of stamp_sheets.
 * @author DBFlute(AutoGenerator)
 */
public class BsStampSheetsCQ extends AbstractBsStampSheetsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected StampSheetsCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsStampSheetsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br>
     * {select ... from ... left outer join (select * from stamp_sheets) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public StampSheetsCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected StampSheetsCIQ xcreateCIQ() {
        StampSheetsCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected StampSheetsCIQ xnewCIQ() {
        return new StampSheetsCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br>
     * {select ... from ... left outer join stamp_sheets on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public StampSheetsCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        StampSheetsCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    protected ConditionValue _id;
    public ConditionValue xdfgetId()
    { if (_id == null) { _id = nCV(); }
      return _id; }
    protected ConditionValue xgetCValueId() { return xdfgetId(); }

    public Map<String, StampsCQ> xdfgetId_ExistsReferrer_StampsList() { return xgetSQueMap("id_ExistsReferrer_StampsList"); }
    public String keepId_ExistsReferrer_StampsList(StampsCQ sq) { return xkeepSQue("id_ExistsReferrer_StampsList", sq); }

    public Map<String, StampsCQ> xdfgetId_NotExistsReferrer_StampsList() { return xgetSQueMap("id_NotExistsReferrer_StampsList"); }
    public String keepId_NotExistsReferrer_StampsList(StampsCQ sq) { return xkeepSQue("id_NotExistsReferrer_StampsList", sq); }

    public Map<String, StampsCQ> xdfgetId_SpecifyDerivedReferrer_StampsList() { return xgetSQueMap("id_SpecifyDerivedReferrer_StampsList"); }
    public String keepId_SpecifyDerivedReferrer_StampsList(StampsCQ sq) { return xkeepSQue("id_SpecifyDerivedReferrer_StampsList", sq); }

    public Map<String, StampsCQ> xdfgetId_QueryDerivedReferrer_StampsList() { return xgetSQueMap("id_QueryDerivedReferrer_StampsList"); }
    public String keepId_QueryDerivedReferrer_StampsList(StampsCQ sq) { return xkeepSQue("id_QueryDerivedReferrer_StampsList", sq); }
    public Map<String, Object> xdfgetId_QueryDerivedReferrer_StampsListParameter() { return xgetSQuePmMap("id_QueryDerivedReferrer_StampsList"); }
    public String keepId_QueryDerivedReferrer_StampsListParameter(Object pm) { return xkeepSQuePm("id_QueryDerivedReferrer_StampsList", pm); }

    /** 
     * Add order-by as ascend. <br>
     * id: {PK, NotNull, varchar(36)}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Id_Asc() { regOBA("id"); return this; }

    /**
     * Add order-by as descend. <br>
     * id: {PK, NotNull, varchar(36)}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Id_Desc() { regOBD("id"); return this; }

    protected ConditionValue _created;
    public ConditionValue xdfgetCreated()
    { if (_created == null) { _created = nCV(); }
      return _created; }
    protected ConditionValue xgetCValueCreated() { return xdfgetCreated(); }

    /** 
     * Add order-by as ascend. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Created_Asc() { regOBA("created"); return this; }

    /**
     * Add order-by as descend. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Created_Desc() { regOBD("created"); return this; }

    protected ConditionValue _updated;
    public ConditionValue xdfgetUpdated()
    { if (_updated == null) { _updated = nCV(); }
      return _updated; }
    protected ConditionValue xgetCValueUpdated() { return xdfgetUpdated(); }

    /** 
     * Add order-by as ascend. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Updated_Asc() { regOBA("updated"); return this; }

    /**
     * Add order-by as descend. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addOrderBy_Updated_Desc() { regOBD("updated"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addSpecifiedDerivedOrderBy_Asc(String aliasName) { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsStampSheetsCQ addSpecifiedDerivedOrderBy_Desc(String aliasName) { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    public void reflectRelationOnUnionQuery(ConditionQuery bqs, ConditionQuery uqs) {
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    public Map<String, StampSheetsCQ> xdfgetScalarCondition() { return xgetSQueMap("scalarCondition"); }
    public String keepScalarCondition(StampSheetsCQ sq) { return xkeepSQue("scalarCondition", sq); }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public Map<String, StampSheetsCQ> xdfgetSpecifyMyselfDerived() { return xgetSQueMap("specifyMyselfDerived"); }
    public String keepSpecifyMyselfDerived(StampSheetsCQ sq) { return xkeepSQue("specifyMyselfDerived", sq); }

    public Map<String, StampSheetsCQ> xdfgetQueryMyselfDerived() { return xgetSQueMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerived(StampSheetsCQ sq) { return xkeepSQue("queryMyselfDerived", sq); }
    public Map<String, Object> xdfgetQueryMyselfDerivedParameter() { return xgetSQuePmMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerivedParameter(Object pm) { return xkeepSQuePm("queryMyselfDerived", pm); }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, StampSheetsCQ> _myselfExistsMap;
    public Map<String, StampSheetsCQ> xdfgetMyselfExists() { return xgetSQueMap("myselfExists"); }
    public String keepMyselfExists(StampSheetsCQ sq) { return xkeepSQue("myselfExists", sq); }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    public Map<String, StampSheetsCQ> xdfgetMyselfInScope() { return xgetSQueMap("myselfInScope"); }
    public String keepMyselfInScope(StampSheetsCQ sq) { return xkeepSQue("myselfInScope", sq); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return StampSheetsCB.class.getName(); }
    protected String xCQ() { return StampSheetsCQ.class.getName(); }
    protected String xCHp() { return HpQDRFunction.class.getName(); }
    protected String xCOp() { return ConditionOption.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
