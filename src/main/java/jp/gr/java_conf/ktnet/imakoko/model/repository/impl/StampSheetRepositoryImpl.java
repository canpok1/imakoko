package jp.gr.java_conf.ktnet.imakoko.model.repository.impl;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.StampSheetsBhv;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.StampSheets;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.StampSheet;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampSheetRepository;
import jp.gr.java_conf.ktnet.imakoko.model.repository.mapper.StampSheetMapper;

import org.dbflute.optional.OptionalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * stamp_sheetsテーブルの情報を操作するクラスです.
 * @author tanabe
 */
@Repository
public class StampSheetRepositoryImpl implements StampSheetRepository {
  
  @Autowired
  private StampSheetMapper mapper;
  
  @Autowired
  private StampSheetsBhv stampSheetBhv;

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository#findAll()
   */
  @Override
  public List<StampSheet> findAll() {
    return stampSheetBhv
              .selectList(cb -> {
                  cb.query().addOrderBy_Id_Desc();
                })
              .mappingList(entity -> new StampSheet(entity));
  }
  
  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository
   * #findById(java.lang.String)
   */
  @Override
  public StampSheet findById(final String id) {
    OptionalEntity<StampSheet> stampSheet = stampSheetBhv
          .selectEntity(cb -> {
              cb.query().setId_Equal(id);
              cb.query().addOrderBy_Id_Desc();
            })
          .map(entity -> new StampSheet(entity));
    if (stampSheet.isPresent()) {
      return stampSheet.get();
    } else {
      return null;
    }
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository#create()
   */
  @Override
  public String create() {
    UUID uuid = UUID.randomUUID();
    LocalDateTime now = LocalDateTime.now();
    
    StampSheets newRecord = new StampSheets();
    newRecord.setId(uuid.toString());
    newRecord.setCreated(now);
    newRecord.setUpdated(now);
    
    stampSheetBhv.insert(newRecord);
    
    return uuid.toString();
  }

  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository
   * #updateCreated(java.lang.String)
   */
  @Override
  public void updateCreated(final String id) {
//    LocalDateTime now = LocalDateTime.now();
//    
//    StampSheets record = new StampSheets();
//    record.setId(id);
//    record.setUpdated(now);
//    
//    stampSheetBhv.update(record);
    mapper.updateUpdated(id);
  }
  
  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository
   * #findInactiveRecord(int)
   */
  @Override
  public List<StampSheet> findInactiveRecord(int activeDateCount) {
    return mapper.findInactiveRecord(activeDateCount);
  }
  
  /* (non-Javadoc)
   * @see jp.gr.java_conf.ktnet.imakoko.model.repository.IStampSheetRepository
   * #deleteById(java.util.List)
   */
  @Override
  public void deleteById(final List<String> idList) {
    if (idList != null && !idList.isEmpty()) {
      mapper.deleteById(idList);
    }
  }
}
