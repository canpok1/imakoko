package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.nss;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.StampsCQ;

/**
 * The nest select set-upper of stamps.
 * @author DBFlute(AutoGenerator)
 */
public class StampsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected final StampsCQ _query;
    public StampsNss(StampsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============
    /**
     * With nested relation columns to select clause. <br>
     * stamp_sheets by my stamp_sheet_id, named 'stampSheets'.
     */
    public void withStampSheets() {
        _query.xdoNss(() -> _query.queryStampSheets());
    }
}
