package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The referrer loader of schema_version as TABLE. <br>
 * <pre>
 * [primary key]
 *     installed_rank
 *
 * [column]
 *     installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success
 *
 * [sequence]
 *     
 *
 * [identity]
 *     
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     
 *
 * [referrer table]
 *     
 *
 * [foreign property]
 *     
 *
 * [referrer property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfSchemaVersion {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<SchemaVersion> _selectedList;
    protected BehaviorSelector _selector;
    protected SchemaVersionBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfSchemaVersion ready(List<SchemaVersion> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected SchemaVersionBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(SchemaVersionBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<SchemaVersion> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
