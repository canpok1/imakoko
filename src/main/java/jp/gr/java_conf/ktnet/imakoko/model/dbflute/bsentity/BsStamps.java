package jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity;

import java.util.List;
import java.util.ArrayList;

import org.dbflute.Entity;
import org.dbflute.dbmeta.DBMeta;
import org.dbflute.dbmeta.AbstractEntity;
import org.dbflute.dbmeta.accessory.DomainEntity;
import org.dbflute.optional.OptionalEntity;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.DBMetaInstanceHandler;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.*;

/**
 * The entity of stamps as TABLE. <br>
 * スタンプテーブル
 * <pre>
 * [primary-key]
 *     id
 *
 * [column]
 *     id, stamp_sheet_id, name, latitude, longitude, created, updated, icon_type
 *
 * [sequence]
 *     
 *
 * [identity]
 *     
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     stamp_sheets
 *
 * [referrer table]
 *     
 *
 * [foreign property]
 *     stampSheets
 *
 * [referrer property]
 *     
 *
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * String id = entity.getId();
 * String stampSheetId = entity.getStampSheetId();
 * String name = entity.getName();
 * String latitude = entity.getLatitude();
 * String longitude = entity.getLongitude();
 * java.time.LocalDateTime created = entity.getCreated();
 * java.time.LocalDateTime updated = entity.getUpdated();
 * Integer iconType = entity.getIconType();
 * entity.setId(id);
 * entity.setStampSheetId(stampSheetId);
 * entity.setName(name);
 * entity.setLatitude(latitude);
 * entity.setLongitude(longitude);
 * entity.setCreated(created);
 * entity.setUpdated(updated);
 * entity.setIconType(iconType);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsStamps extends AbstractEntity implements DomainEntity {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    /** id: {PK, NotNull, varchar(36)} */
    protected String _id;

    /** stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets} */
    protected String _stampSheetId;

    /** name: {NotNull, varchar(64)} */
    protected String _name;

    /** latitude: {NotNull, text(2147483647)} */
    protected String _latitude;

    /** longitude: {NotNull, text(2147483647)} */
    protected String _longitude;

    /** created: {NotNull, timestamp(29, 6), default=[now()]} */
    protected java.time.LocalDateTime _created;

    /** updated: {NotNull, timestamp(29, 6), default=[now()]} */
    protected java.time.LocalDateTime _updated;

    /** icon_type: {int4(10), default=[0]} */
    protected Integer _iconType;

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    /** {@inheritDoc} */
    public DBMeta asDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(asTableDbName());
    }

    /** {@inheritDoc} */
    public String asTableDbName() {
        return "stamps";
    }

    // ===================================================================================
    //                                                                        Key Handling
    //                                                                        ============
    /** {@inheritDoc} */
    public boolean hasPrimaryKeyValue() {
        if (_id == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    /** stamp_sheets by my stamp_sheet_id, named 'stampSheets'. */
    protected OptionalEntity<StampSheets> _stampSheets;

    /**
     * [get] stamp_sheets by my stamp_sheet_id, named 'stampSheets'. <br>
     * Optional: alwaysPresent(), ifPresent().orElse(), get(), ...
     * @return The entity of foreign property 'stampSheets'. (NotNull, EmptyAllowed: when e.g. null FK column, no setupSelect)
     */
    public OptionalEntity<StampSheets> getStampSheets() {
        if (_stampSheets == null) { _stampSheets = OptionalEntity.relationEmpty(this, "stampSheets"); }
        return _stampSheets;
    }

    /**
     * [set] stamp_sheets by my stamp_sheet_id, named 'stampSheets'.
     * @param stampSheets The entity of foreign property 'stampSheets'. (NullAllowed)
     */
    public void setStampSheets(OptionalEntity<StampSheets> stampSheets) {
        _stampSheets = stampSheets;
    }

    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    protected <ELEMENT> List<ELEMENT> newReferrerList() { // overriding to import
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    @Override
    protected boolean doEquals(Object obj) {
        if (obj instanceof BsStamps) {
            BsStamps other = (BsStamps)obj;
            if (!xSV(_id, other._id)) { return false; }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected int doHashCode(int initial) {
        int hs = initial;
        hs = xCH(hs, asTableDbName());
        hs = xCH(hs, _id);
        return hs;
    }

    @Override
    protected String doBuildStringWithRelation(String li) {
        StringBuilder sb = new StringBuilder();
        if (_stampSheets != null && _stampSheets.isPresent())
        { sb.append(li).append(xbRDS(_stampSheets, "stampSheets")); }
        return sb.toString();
    }
    protected <ET extends Entity> String xbRDS(org.dbflute.optional.OptionalEntity<ET> et, String name) { // buildRelationDisplayString()
        return et.get().buildDisplayString(name, true, true);
    }

    @Override
    protected String doBuildColumnString(String dm) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm).append(xfND(_id));
        sb.append(dm).append(xfND(_stampSheetId));
        sb.append(dm).append(xfND(_name));
        sb.append(dm).append(xfND(_latitude));
        sb.append(dm).append(xfND(_longitude));
        sb.append(dm).append(xfND(_created));
        sb.append(dm).append(xfND(_updated));
        sb.append(dm).append(xfND(_iconType));
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    @Override
    protected String doBuildRelationString(String dm) {
        StringBuilder sb = new StringBuilder();
        if (_stampSheets != null && _stampSheets.isPresent())
        { sb.append(dm).append("stampSheets"); }
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length()).insert(0, "(").append(")");
        }
        return sb.toString();
    }

    @Override
    public Stamps clone() {
        return (Stamps)super.clone();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] id: {PK, NotNull, varchar(36)} <br>
     * ID
     * @return The value of the column 'id'. (basically NotNull if selected: for the constraint)
     */
    public String getId() {
        checkSpecifiedProperty("id");
        return _id;
    }

    /**
     * [set] id: {PK, NotNull, varchar(36)} <br>
     * ID
     * @param id The value of the column 'id'. (basically NotNull if update: for the constraint)
     */
    public void setId(String id) {
        registerModifiedProperty("id");
        _id = id;
    }

    /**
     * [get] stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets} <br>
     * シートID
     * @return The value of the column 'stamp_sheet_id'. (basically NotNull if selected: for the constraint)
     */
    public String getStampSheetId() {
        checkSpecifiedProperty("stampSheetId");
        return _stampSheetId;
    }

    /**
     * [set] stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets} <br>
     * シートID
     * @param stampSheetId The value of the column 'stamp_sheet_id'. (basically NotNull if update: for the constraint)
     */
    public void setStampSheetId(String stampSheetId) {
        registerModifiedProperty("stampSheetId");
        _stampSheetId = stampSheetId;
    }

    /**
     * [get] name: {NotNull, varchar(64)} <br>
     * 名前
     * @return The value of the column 'name'. (basically NotNull if selected: for the constraint)
     */
    public String getName() {
        checkSpecifiedProperty("name");
        return _name;
    }

    /**
     * [set] name: {NotNull, varchar(64)} <br>
     * 名前
     * @param name The value of the column 'name'. (basically NotNull if update: for the constraint)
     */
    public void setName(String name) {
        registerModifiedProperty("name");
        _name = name;
    }

    /**
     * [get] latitude: {NotNull, text(2147483647)} <br>
     * 緯度
     * @return The value of the column 'latitude'. (basically NotNull if selected: for the constraint)
     */
    public String getLatitude() {
        checkSpecifiedProperty("latitude");
        return _latitude;
    }

    /**
     * [set] latitude: {NotNull, text(2147483647)} <br>
     * 緯度
     * @param latitude The value of the column 'latitude'. (basically NotNull if update: for the constraint)
     */
    public void setLatitude(String latitude) {
        registerModifiedProperty("latitude");
        _latitude = latitude;
    }

    /**
     * [get] longitude: {NotNull, text(2147483647)} <br>
     * 軽度
     * @return The value of the column 'longitude'. (basically NotNull if selected: for the constraint)
     */
    public String getLongitude() {
        checkSpecifiedProperty("longitude");
        return _longitude;
    }

    /**
     * [set] longitude: {NotNull, text(2147483647)} <br>
     * 軽度
     * @param longitude The value of the column 'longitude'. (basically NotNull if update: for the constraint)
     */
    public void setLongitude(String longitude) {
        registerModifiedProperty("longitude");
        _longitude = longitude;
    }

    /**
     * [get] created: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 作成日時
     * @return The value of the column 'created'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getCreated() {
        checkSpecifiedProperty("created");
        return _created;
    }

    /**
     * [set] created: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 作成日時
     * @param created The value of the column 'created'. (basically NotNull if update: for the constraint)
     */
    public void setCreated(java.time.LocalDateTime created) {
        registerModifiedProperty("created");
        _created = created;
    }

    /**
     * [get] updated: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 更新日時
     * @return The value of the column 'updated'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getUpdated() {
        checkSpecifiedProperty("updated");
        return _updated;
    }

    /**
     * [set] updated: {NotNull, timestamp(29, 6), default=[now()]} <br>
     * 更新日時
     * @param updated The value of the column 'updated'. (basically NotNull if update: for the constraint)
     */
    public void setUpdated(java.time.LocalDateTime updated) {
        registerModifiedProperty("updated");
        _updated = updated;
    }

    /**
     * [get] icon_type: {int4(10), default=[0]} <br>
     * アイコン種別<br>
     * 0 : いまここ<br>
     * 1〜10 : マーカー1〜10
     * @return The value of the column 'icon_type'. (NullAllowed even if selected: for no constraint)
     */
    public Integer getIconType() {
        checkSpecifiedProperty("iconType");
        return _iconType;
    }

    /**
     * [set] icon_type: {int4(10), default=[0]} <br>
     * アイコン種別<br>
     * 0 : いまここ<br>
     * 1〜10 : マーカー1〜10
     * @param iconType The value of the column 'icon_type'. (NullAllowed: null update allowed for no constraint)
     */
    public void setIconType(Integer iconType) {
        registerModifiedProperty("iconType");
        _iconType = iconType;
    }
}
