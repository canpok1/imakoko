package jp.gr.java_conf.ktnet.imakoko.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * エラーページのテンプレートを定義するクラスです.
 * @author tanabe
 *
 */
@Controller
public class ErrorController {

  @RequestMapping(value = "/404")
  public String getNotFoundTemplate() {
    return "error/404";
  }
  
  @RequestMapping(value = "/500")
  public String getInternalServerErrorTemplate() {
    return "error/500";
  }
  
  @RequestMapping(value = "/503")
  public String getServiceUnavailable() {
    return "error/503";
  }
}
