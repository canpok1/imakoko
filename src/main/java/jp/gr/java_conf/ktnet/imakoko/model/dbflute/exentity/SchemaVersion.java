package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsentity.BsSchemaVersion;

/**
 * The entity of schema_version.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaVersion extends BsSchemaVersion {

    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;
}
