package jp.gr.java_conf.ktnet.imakoko.controller;

import jp.gr.java_conf.ktnet.imakoko.config.EnvironmentValues;
import jp.gr.java_conf.ktnet.imakoko.model.StampSheetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
@Slf4j
public class StampSheetController {
  
  @Autowired
  private StampSheetService stampSheetService;

  @Autowired
  private EnvironmentValues environmentValues;
  
  /**
   * スタンプシートのテンプレート名を取得します.
   * @param model モデル.
   * @param stampSheetId スタンプシートID.
   * @return HTMLテンプレート名.
   */
  @RequestMapping("/stampsheet")
  public String showPage(
                                 final Model model,
      @RequestParam("id")        final String stampSheetId,
      @RequestParam("latitude")  final Optional<String> latitude,
      @RequestParam("longitude") final Optional<String> longitude,
      @RequestParam("stamp")     final Optional<String> stampId
  ) {
    
    if (stampSheetService.findById(stampSheetId) == null) {
      log.warn("id is invalid [" + stampSheetId + "]");
      return "map-not-found";
    }
    
    model.addAttribute("stampSheetId", stampSheetId);
    
    if (latitude.isPresent() && longitude.isPresent()) {
      model.addAttribute("latitude", latitude.get());
      model.addAttribute("longitude", longitude.get());
      model.addAttribute("traceMode", "fixed");
    } else {
      model.addAttribute("traceMode", "trace");
    }
    if (stampId.isPresent()) {
      model.addAttribute("openStampId", stampId.get());
    }
    
    return "stampsheet";
  }

}
