package jp.gr.java_conf.ktnet.imakoko.model.entity.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.exentity.Stamps;
import jp.gr.java_conf.ktnet.imakoko.model.entity.serializer.DateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stamp {
  @ApiModelProperty(value = "ID", required = true)
  @JsonProperty("id")
  private String id;
  
  @ApiModelProperty(value = "マップのID", required = true)
  @JsonProperty("stamp_sheet_id")
  private String stampSheetId;
  
  @ApiModelProperty(value = "作成者の名前", required = true)
  @JsonProperty("name")
  private String name;
  
  @ApiModelProperty(value = "緯度", required = true)
  @JsonProperty("latitude")
  private String latitude;
  
  @ApiModelProperty(value = "経度", required = true)
  @JsonProperty("longitude")
  private String longitude;
  
  @ApiModelProperty(value = "アイコン種別（0:いまここ, 1〜10:マーカー1〜10）")
  @JsonProperty("icon_type")
  private int iconType;
  
  @ApiModelProperty(value = "作成日時")
  @JsonIgnore
  private Date created;
  
  @ApiModelProperty(value = "更新日時")
  @JsonSerialize(using = DateSerializer.class)
  @JsonProperty("updated")
  private Date updated;
  
  /**
   * コピーコンストラクタ.
   * 同じ値を持つ新たなインスタンスを生成します.
   * @param original コピー元.
   */
  public Stamp(@NonNull final Stamp original) {
    this(
        original.getId(),
        original.getStampSheetId(),
        original.getName(),
        original.getLatitude(),
        original.getLongitude(),
        original.getIconType(),
        original.getCreated(),
        original.getUpdated()
        );
  }
  
  /**
   * エンティティクラスを元にインスタンスを生成します.
   * @param entity エンティティクラス.
   */
  public Stamp(@NonNull final Stamps entity) {
    this(
        entity.getId(),
        entity.getStampSheetId(),
        entity.getName(),
        entity.getLatitude(),
        entity.getLongitude(),
        entity.getIconType(),
        null,
        null
    );
  }
}
