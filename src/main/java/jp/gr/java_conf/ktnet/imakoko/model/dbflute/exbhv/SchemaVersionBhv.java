package jp.gr.java_conf.ktnet.imakoko.model.dbflute.exbhv;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.bsbhv.BsSchemaVersionBhv;

/**
 * The behavior of schema_version.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("schemaVersionBhv")
public class SchemaVersionBhv extends BsSchemaVersionBhv {
}
