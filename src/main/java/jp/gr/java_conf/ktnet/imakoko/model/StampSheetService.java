package jp.gr.java_conf.ktnet.imakoko.model;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.StampSheet;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampRepository;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampSheetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * スタンプ関連の処理を行うサービスクラスです.
 * @author tanabe
 */
@Service
@Slf4j
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class StampSheetService {

  @Autowired
  private StampSheetRepository stampSheetRepository;
  
  @Autowired
  private StampRepository stampRepository;
  
  /**
   * 指定IDのレコードを取得します.
   * @param id ID.
   * @return レコード.
   */
  public StampSheet findById(final String id) {
    return stampSheetRepository.findById(id);
  }
  
  /**
   * レコードを新規作成します.
   * @return 新規レコードに割り振られたID.
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public String create() {
    return stampSheetRepository.create();
  }
  
  /**
   * 無効なスタンプシートを削除します.
   * スタンプシートの更新日が有効日数より古い場合は無効と判断します.
   * 例)更新日が「2016/08/10 15:00:00のスタンプシート。
   * 有効日数が1 → 2016/08/11 15:00:00を迎えると無効になります.
   * 有効日数が0 → すぐに無効になります.
   * @param activeDateCount スタンプの有効日数.
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void deleteInactiveStamp(int activeDateCount) {
    List<StampSheet> inactiveSheetList = stampSheetRepository.findInactiveRecord(activeDateCount);
    
    List<String> inactiveSheetIdList = new ArrayList<>();
    
    for (StampSheet inactiveSheet : inactiveSheetList) {
      inactiveSheetIdList.add(inactiveSheet.getId());
      log.info("[削除対象]{}", inactiveSheet);
    }
    
    stampRepository.deleteByStampSheetId(inactiveSheetIdList);
    stampSheetRepository.deleteById(inactiveSheetIdList);
  }
}
