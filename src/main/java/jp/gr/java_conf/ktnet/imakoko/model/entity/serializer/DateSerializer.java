package jp.gr.java_conf.ktnet.imakoko.model.entity.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;


/**
 * Date型変数をJSON文字列へと変換する方法を定義するクラスです.
 * @author tanabe
 *
 */
public class DateSerializer extends JsonSerializer<Date> {
    
  @Override
  public void serialize(
      Date value,
      JsonGenerator jgen,
      SerializerProvider provider)
      throws IOException, JsonProcessingException {
    jgen.writeString(new DateTime(value).toString());
  }

}
