package jp.gr.java_conf.ktnet.imakoko.model.repository.mapper;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * stampsテーブルからのレコード取得方法を定義するインターフェースです.
 * @author tanabe
 */
public interface StampMapper {
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " stamp_sheet_id,"
      + " name,"
      + " latitude,"
      + " longitude,"
      + " icon_type,"
      + " created,"
      + " updated "
      + "FROM"
      + " imakoko.stamps")
  public List<Stamp> findAll();
  
  /**
   * 指定のIDに一致するレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " stamp_sheet_id,"
      + " name,"
      + " latitude,"
      + " longitude,"
      + " icon_type,"
      + " created,"
      + " updated "
      + "FROM"
      + " imakoko.stamps "
      + "WHERE"
      + " id = #{id}"
      + " AND stamp_sheet_id = #{stampSheetId}")
  public Stamp findById(
      @Param("id") final String id,
      @Param("stampSheetId") final String stampSheetId
      );

  /**
   * 指定のスタンプシートIDに一致するレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " stamp_sheet_id,"
      + " name,"
      + " latitude,"
      + " longitude,"
      + " icon_type,"
      + " created,"
      + " updated "
      + "FROM"
      + " imakoko.stamps "
      + "WHERE"
      + " stamp_sheet_id = #{stampSheetId}")
  public List<Stamp> findByStampSheetId(String stampSheetId);
  
  /**
   * レコードを追加します.
   * @return 追加したレコード数.
   */
  @Insert(
      "INSERT INTO imakoko.stamps ("
      + " id,"
      + " stamp_sheet_id,"
      + " name,"
      + " latitude,"
      + " longitude,"
      + " icon_type,"
      + " created,"
      + " updated "
      + ") VALUES ("
      + " #{id},"
      + " #{stampSheetId},"
      + " #{name},"
      + " #{latitude},"
      + " #{longitude},"
      + " #{iconType},"
      + " NOW(),"
      + " NOW() "
      + ")")
  public int insert(Stamp record);
  
  /**
   * レコードを追加します.
   * @return 追加したレコード数.
   */
  @Update(
      "UPDATE imakoko.stamps SET "
      + " stamp_sheet_id = #{stampSheetId},"
      + " name = #{name},"
      + " latitude = #{latitude},"
      + " longitude = #{longitude},"
      + " icon_type = #{iconType},"
      + " updated = NOW() "
      + "WHERE"
      + " id = #{id}"
      )
  public int update(Stamp record);

  /**
   * 指定のスタンプシートIDのレコードを削除します.
   * @param idList スタンプシートID.
   */
  @Delete(
      "<script>"
      + "DELETE "
      + "FROM imakoko.stamps "
      + "WHERE "
      + " stamp_sheet_id IN "
      + "<foreach item='item' collection='idList' open='(' separator=',' close=')'>"
      + "#{item}"
      + "</foreach>"
      + "</script>")
  public void deleteByStampSheetId(@Param("idList") List<String> idList);

}
