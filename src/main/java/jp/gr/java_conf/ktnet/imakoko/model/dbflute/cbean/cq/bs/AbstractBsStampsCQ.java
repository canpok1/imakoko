package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.bs;

import java.util.*;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.ordering.*;
import org.dbflute.cbean.scoping.*;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.dbmeta.DBMetaProvider;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of stamps.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsStampsCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsStampsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    public String asTableDbName() {
        return "stamps";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_Equal(String id) {
        doSetId_Equal(fRES(id));
    }

    protected void doSetId_Equal(String id) {
        regId(CK_EQ, id);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_NotEqual(String id) {
        doSetId_NotEqual(fRES(id));
    }

    protected void doSetId_NotEqual(String id) {
        regId(CK_NES, id);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_GreaterThan(String id) {
        regId(CK_GT, fRES(id));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_LessThan(String id) {
        regId(CK_LT, fRES(id));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_GreaterEqual(String id) {
        regId(CK_GE, fRES(id));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_LessEqual(String id) {
        regId(CK_LE, fRES(id));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param idList The collection of id as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_InScope(Collection<String> idList) {
        doSetId_InScope(idList);
    }

    protected void doSetId_InScope(Collection<String> idList) {
        regINS(CK_INS, cTL(idList), xgetCValueId(), "id");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param idList The collection of id as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_NotInScope(Collection<String> idList) {
        doSetId_NotInScope(idList);
    }

    protected void doSetId_NotInScope(Collection<String> idList) {
        regINS(CK_NINS, cTL(idList), xgetCValueId(), "id");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)} <br>
     * <pre>e.g. setId_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param id The value of id as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setId_LikeSearch(String id, ConditionOptionCall<LikeSearchOption> opLambda) {
        setId_LikeSearch(id, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)} <br>
     * <pre>e.g. setId_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param id The value of id as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setId_LikeSearch(String id, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(id), xgetCValueId(), "id", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setId_NotLikeSearch(String id, ConditionOptionCall<LikeSearchOption> opLambda) {
        setId_NotLikeSearch(id, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     * @param id The value of id as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setId_NotLikeSearch(String id, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(id), xgetCValueId(), "id", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     */
    public void setId_IsNull() { regId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * id: {PK, NotNull, varchar(36)}
     */
    public void setId_IsNotNull() { regId(CK_ISNN, DOBJ); }

    protected void regId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueId(), "id"); }
    protected abstract ConditionValue xgetCValueId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_Equal(String stampSheetId) {
        doSetStampSheetId_Equal(fRES(stampSheetId));
    }

    protected void doSetStampSheetId_Equal(String stampSheetId) {
        regStampSheetId(CK_EQ, stampSheetId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_NotEqual(String stampSheetId) {
        doSetStampSheetId_NotEqual(fRES(stampSheetId));
    }

    protected void doSetStampSheetId_NotEqual(String stampSheetId) {
        regStampSheetId(CK_NES, stampSheetId);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_GreaterThan(String stampSheetId) {
        regStampSheetId(CK_GT, fRES(stampSheetId));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_LessThan(String stampSheetId) {
        regStampSheetId(CK_LT, fRES(stampSheetId));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_GreaterEqual(String stampSheetId) {
        regStampSheetId(CK_GE, fRES(stampSheetId));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_LessEqual(String stampSheetId) {
        regStampSheetId(CK_LE, fRES(stampSheetId));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetIdList The collection of stampSheetId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_InScope(Collection<String> stampSheetIdList) {
        doSetStampSheetId_InScope(stampSheetIdList);
    }

    protected void doSetStampSheetId_InScope(Collection<String> stampSheetIdList) {
        regINS(CK_INS, cTL(stampSheetIdList), xgetCValueStampSheetId(), "stamp_sheet_id");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetIdList The collection of stampSheetId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setStampSheetId_NotInScope(Collection<String> stampSheetIdList) {
        doSetStampSheetId_NotInScope(stampSheetIdList);
    }

    protected void doSetStampSheetId_NotInScope(Collection<String> stampSheetIdList) {
        regINS(CK_NINS, cTL(stampSheetIdList), xgetCValueStampSheetId(), "stamp_sheet_id");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets} <br>
     * <pre>e.g. setStampSheetId_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param stampSheetId The value of stampSheetId as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setStampSheetId_LikeSearch(String stampSheetId, ConditionOptionCall<LikeSearchOption> opLambda) {
        setStampSheetId_LikeSearch(stampSheetId, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets} <br>
     * <pre>e.g. setStampSheetId_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param stampSheetId The value of stampSheetId as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setStampSheetId_LikeSearch(String stampSheetId, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(stampSheetId), xgetCValueStampSheetId(), "stamp_sheet_id", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setStampSheetId_NotLikeSearch(String stampSheetId, ConditionOptionCall<LikeSearchOption> opLambda) {
        setStampSheetId_NotLikeSearch(stampSheetId, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @param stampSheetId The value of stampSheetId as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setStampSheetId_NotLikeSearch(String stampSheetId, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(stampSheetId), xgetCValueStampSheetId(), "stamp_sheet_id", likeSearchOption);
    }

    protected void regStampSheetId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueStampSheetId(), "stamp_sheet_id"); }
    protected abstract ConditionValue xgetCValueStampSheetId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_Equal(String name) {
        doSetName_Equal(fRES(name));
    }

    protected void doSetName_Equal(String name) {
        regName(CK_EQ, name);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_NotEqual(String name) {
        doSetName_NotEqual(fRES(name));
    }

    protected void doSetName_NotEqual(String name) {
        regName(CK_NES, name);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_GreaterThan(String name) {
        regName(CK_GT, fRES(name));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_LessThan(String name) {
        regName(CK_LT, fRES(name));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_GreaterEqual(String name) {
        regName(CK_GE, fRES(name));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_LessEqual(String name) {
        regName(CK_LE, fRES(name));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param nameList The collection of name as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_InScope(Collection<String> nameList) {
        doSetName_InScope(nameList);
    }

    protected void doSetName_InScope(Collection<String> nameList) {
        regINS(CK_INS, cTL(nameList), xgetCValueName(), "name");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param nameList The collection of name as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_NotInScope(Collection<String> nameList) {
        doSetName_NotInScope(nameList);
    }

    protected void doSetName_NotInScope(Collection<String> nameList) {
        regINS(CK_NINS, cTL(nameList), xgetCValueName(), "name");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)} <br>
     * <pre>e.g. setName_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param name The value of name as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setName_LikeSearch(String name, ConditionOptionCall<LikeSearchOption> opLambda) {
        setName_LikeSearch(name, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)} <br>
     * <pre>e.g. setName_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param name The value of name as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setName_LikeSearch(String name, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(name), xgetCValueName(), "name", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setName_NotLikeSearch(String name, ConditionOptionCall<LikeSearchOption> opLambda) {
        setName_NotLikeSearch(name, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * name: {NotNull, varchar(64)}
     * @param name The value of name as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setName_NotLikeSearch(String name, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(name), xgetCValueName(), "name", likeSearchOption);
    }

    protected void regName(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueName(), "name"); }
    protected abstract ConditionValue xgetCValueName();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_Equal(String latitude) {
        doSetLatitude_Equal(fRES(latitude));
    }

    protected void doSetLatitude_Equal(String latitude) {
        regLatitude(CK_EQ, latitude);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_NotEqual(String latitude) {
        doSetLatitude_NotEqual(fRES(latitude));
    }

    protected void doSetLatitude_NotEqual(String latitude) {
        regLatitude(CK_NES, latitude);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_GreaterThan(String latitude) {
        regLatitude(CK_GT, fRES(latitude));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_LessThan(String latitude) {
        regLatitude(CK_LT, fRES(latitude));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_GreaterEqual(String latitude) {
        regLatitude(CK_GE, fRES(latitude));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_LessEqual(String latitude) {
        regLatitude(CK_LE, fRES(latitude));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitudeList The collection of latitude as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_InScope(Collection<String> latitudeList) {
        doSetLatitude_InScope(latitudeList);
    }

    protected void doSetLatitude_InScope(Collection<String> latitudeList) {
        regINS(CK_INS, cTL(latitudeList), xgetCValueLatitude(), "latitude");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitudeList The collection of latitude as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setLatitude_NotInScope(Collection<String> latitudeList) {
        doSetLatitude_NotInScope(latitudeList);
    }

    protected void doSetLatitude_NotInScope(Collection<String> latitudeList) {
        regINS(CK_NINS, cTL(latitudeList), xgetCValueLatitude(), "latitude");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)} <br>
     * <pre>e.g. setLatitude_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param latitude The value of latitude as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setLatitude_LikeSearch(String latitude, ConditionOptionCall<LikeSearchOption> opLambda) {
        setLatitude_LikeSearch(latitude, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)} <br>
     * <pre>e.g. setLatitude_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param latitude The value of latitude as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setLatitude_LikeSearch(String latitude, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(latitude), xgetCValueLatitude(), "latitude", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setLatitude_NotLikeSearch(String latitude, ConditionOptionCall<LikeSearchOption> opLambda) {
        setLatitude_NotLikeSearch(latitude, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * latitude: {NotNull, text(2147483647)}
     * @param latitude The value of latitude as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setLatitude_NotLikeSearch(String latitude, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(latitude), xgetCValueLatitude(), "latitude", likeSearchOption);
    }

    protected void regLatitude(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueLatitude(), "latitude"); }
    protected abstract ConditionValue xgetCValueLatitude();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_Equal(String longitude) {
        doSetLongitude_Equal(fRES(longitude));
    }

    protected void doSetLongitude_Equal(String longitude) {
        regLongitude(CK_EQ, longitude);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_NotEqual(String longitude) {
        doSetLongitude_NotEqual(fRES(longitude));
    }

    protected void doSetLongitude_NotEqual(String longitude) {
        regLongitude(CK_NES, longitude);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_GreaterThan(String longitude) {
        regLongitude(CK_GT, fRES(longitude));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_LessThan(String longitude) {
        regLongitude(CK_LT, fRES(longitude));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_GreaterEqual(String longitude) {
        regLongitude(CK_GE, fRES(longitude));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_LessEqual(String longitude) {
        regLongitude(CK_LE, fRES(longitude));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitudeList The collection of longitude as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_InScope(Collection<String> longitudeList) {
        doSetLongitude_InScope(longitudeList);
    }

    protected void doSetLongitude_InScope(Collection<String> longitudeList) {
        regINS(CK_INS, cTL(longitudeList), xgetCValueLongitude(), "longitude");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitudeList The collection of longitude as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setLongitude_NotInScope(Collection<String> longitudeList) {
        doSetLongitude_NotInScope(longitudeList);
    }

    protected void doSetLongitude_NotInScope(Collection<String> longitudeList) {
        regINS(CK_NINS, cTL(longitudeList), xgetCValueLongitude(), "longitude");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)} <br>
     * <pre>e.g. setLongitude_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param longitude The value of longitude as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setLongitude_LikeSearch(String longitude, ConditionOptionCall<LikeSearchOption> opLambda) {
        setLongitude_LikeSearch(longitude, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)} <br>
     * <pre>e.g. setLongitude_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param longitude The value of longitude as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setLongitude_LikeSearch(String longitude, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(longitude), xgetCValueLongitude(), "longitude", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setLongitude_NotLikeSearch(String longitude, ConditionOptionCall<LikeSearchOption> opLambda) {
        setLongitude_NotLikeSearch(longitude, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * longitude: {NotNull, text(2147483647)}
     * @param longitude The value of longitude as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setLongitude_NotLikeSearch(String longitude, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(longitude), xgetCValueLongitude(), "longitude", likeSearchOption);
    }

    protected void regLongitude(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueLongitude(), "longitude"); }
    protected abstract ConditionValue xgetCValueLongitude();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @param created The value of created as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreated_Equal(java.time.LocalDateTime created) {
        regCreated(CK_EQ,  created);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @param created The value of created as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreated_GreaterThan(java.time.LocalDateTime created) {
        regCreated(CK_GT,  created);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @param created The value of created as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreated_LessThan(java.time.LocalDateTime created) {
        regCreated(CK_LT,  created);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @param created The value of created as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreated_GreaterEqual(java.time.LocalDateTime created) {
        regCreated(CK_GE,  created);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @param created The value of created as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreated_LessEqual(java.time.LocalDateTime created) {
        regCreated(CK_LE, created);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * <pre>e.g. setCreated_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of created. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of created. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setCreated_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setCreated_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * <pre>e.g. setCreated_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of created. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of created. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setCreated_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "created"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueCreated(), nm, op);
    }

    protected void regCreated(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueCreated(), "created"); }
    protected abstract ConditionValue xgetCValueCreated();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @param updated The value of updated as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdated_Equal(java.time.LocalDateTime updated) {
        regUpdated(CK_EQ,  updated);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @param updated The value of updated as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdated_GreaterThan(java.time.LocalDateTime updated) {
        regUpdated(CK_GT,  updated);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @param updated The value of updated as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdated_LessThan(java.time.LocalDateTime updated) {
        regUpdated(CK_LT,  updated);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @param updated The value of updated as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdated_GreaterEqual(java.time.LocalDateTime updated) {
        regUpdated(CK_GE,  updated);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @param updated The value of updated as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdated_LessEqual(java.time.LocalDateTime updated) {
        regUpdated(CK_LE, updated);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * <pre>e.g. setUpdated_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updated. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updated. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setUpdated_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setUpdated_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * <pre>e.g. setUpdated_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updated. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updated. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setUpdated_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "updated"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueUpdated(), nm, op);
    }

    protected void regUpdated(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUpdated(), "updated"); }
    protected abstract ConditionValue xgetCValueUpdated();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_Equal(Integer iconType) {
        doSetIconType_Equal(iconType);
    }

    protected void doSetIconType_Equal(Integer iconType) {
        regIconType(CK_EQ, iconType);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_NotEqual(Integer iconType) {
        doSetIconType_NotEqual(iconType);
    }

    protected void doSetIconType_NotEqual(Integer iconType) {
        regIconType(CK_NES, iconType);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_GreaterThan(Integer iconType) {
        regIconType(CK_GT, iconType);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_LessThan(Integer iconType) {
        regIconType(CK_LT, iconType);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_GreaterEqual(Integer iconType) {
        regIconType(CK_GE, iconType);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconType The value of iconType as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setIconType_LessEqual(Integer iconType) {
        regIconType(CK_LE, iconType);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param minNumber The min number of iconType. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of iconType. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setIconType_RangeOf(Integer minNumber, Integer maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setIconType_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param minNumber The min number of iconType. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of iconType. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setIconType_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueIconType(), "icon_type", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconTypeList The collection of iconType as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconType_InScope(Collection<Integer> iconTypeList) {
        doSetIconType_InScope(iconTypeList);
    }

    protected void doSetIconType_InScope(Collection<Integer> iconTypeList) {
        regINS(CK_INS, cTL(iconTypeList), xgetCValueIconType(), "icon_type");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     * @param iconTypeList The collection of iconType as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconType_NotInScope(Collection<Integer> iconTypeList) {
        doSetIconType_NotInScope(iconTypeList);
    }

    protected void doSetIconType_NotInScope(Collection<Integer> iconTypeList) {
        regINS(CK_NINS, cTL(iconTypeList), xgetCValueIconType(), "icon_type");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     */
    public void setIconType_IsNull() { regIconType(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * icon_type: {int4(10), default=[0]}
     */
    public void setIconType_IsNotNull() { regIconType(CK_ISNN, DOBJ); }

    protected void regIconType(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueIconType(), "icon_type"); }
    protected abstract ConditionValue xgetCValueIconType();

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO = (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_Equal() {
        return xcreateSLCFunction(CK_EQ, StampsCB.class);
    }

    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO &lt;&gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_NotEqual() {
        return xcreateSLCFunction(CK_NES, StampsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterThan. <br>
     * {where FOO &gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_GreaterThan() {
        return xcreateSLCFunction(CK_GT, StampsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessThan. <br>
     * {where FOO &lt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_LessThan() {
        return xcreateSLCFunction(CK_LT, StampsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterEqual. <br>
     * {where FOO &gt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_GreaterEqual() {
        return xcreateSLCFunction(CK_GE, StampsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessEqual. <br>
     * {where FOO &lt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().<span style="color: #CC4747">scalar_LessEqual()</span>.max(new SubQuery&lt;StampsCB&gt;() {
     *     public void query(StampsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<StampsCB> scalar_LessEqual() {
        return xcreateSLCFunction(CK_LE, StampsCB.class);
    }

    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xscalarCondition(String fn, SubQuery<CB> sq, String rd, HpSLCCustomized<CB> cs, ScalarConditionOption op) {
        assertObjectNotNull("subQuery", sq);
        StampsCB cb = xcreateScalarConditionCB(); sq.query((CB)cb);
        String pp = keepScalarCondition(cb.query()); // for saving query-value
        cs.setPartitionByCBean((CB)xcreateScalarConditionPartitionByCB()); // for using partition-by
        registerScalarCondition(fn, cb.query(), pp, rd, cs, op);
    }
    public abstract String keepScalarCondition(StampsCQ sq);

    protected StampsCB xcreateScalarConditionCB() {
        StampsCB cb = newMyCB(); cb.xsetupForScalarCondition(this); return cb;
    }

    protected StampsCB xcreateScalarConditionPartitionByCB() {
        StampsCB cb = newMyCB(); cb.xsetupForScalarConditionPartitionBy(this); return cb;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public void xsmyselfDerive(String fn, SubQuery<StampsCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        StampsCB cb = new StampsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepSpecifyMyselfDerived(cb.query()); String pk = "id";
        registerSpecifyMyselfDerived(fn, cb.query(), pk, pk, pp, "myselfDerived", al, op);
    }
    public abstract String keepSpecifyMyselfDerived(StampsCQ sq);

    /**
     * Prepare for (Query)MyselfDerived (correlated sub-query).
     * @return The object to set up a function for myself table. (NotNull)
     */
    public HpQDRFunction<StampsCB> myselfDerived() {
        return xcreateQDRFunctionMyselfDerived(StampsCB.class);
    }
    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xqderiveMyselfDerived(String fn, SubQuery<CB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        StampsCB cb = new StampsCB(); cb.xsetupForDerivedReferrer(this); sq.query((CB)cb);
        String pk = "id";
        String sqpp = keepQueryMyselfDerived(cb.query()); // for saving query-value.
        String prpp = keepQueryMyselfDerivedParameter(vl);
        registerQueryMyselfDerived(fn, cb.query(), pk, pk, sqpp, "myselfDerived", rd, vl, prpp, op);
    }
    public abstract String keepQueryMyselfDerived(StampsCQ sq);
    public abstract String keepQueryMyselfDerivedParameter(Object vl);

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    /**
     * Prepare for MyselfExists (correlated sub-query).
     * @param subCBLambda The implementation of sub-query. (NotNull)
     */
    public void myselfExists(SubQuery<StampsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        StampsCB cb = new StampsCB(); cb.xsetupForMyselfExists(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepMyselfExists(cb.query());
        registerMyselfExists(cb.query(), pp);
    }
    public abstract String keepMyselfExists(StampsCQ sq);

    // ===================================================================================
    //                                                                        Manual Order
    //                                                                        ============
    /**
     * Order along manual ordering information.
     * <pre>
     * cb.query().addOrderBy_Birthdate_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when BIRTHDATE &gt;= '2000/01/01' then 0</span>
     * <span style="color: #3F7E5E">//     else 1</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     *
     * cb.query().addOrderBy_MemberStatusCode_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Withdrawal);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Formalized);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Provisional);
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'WDL' then 0</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'FML' then 1</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'PRV' then 2</span>
     * <span style="color: #3F7E5E">//     else 3</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     * </pre>
     * <p>This function with Union is unsupported!</p>
     * <p>The order values are bound (treated as bind parameter).</p>
     * @param opLambda The callback for option of manual-order containing order values. (NotNull)
     */
    public void withManualOrder(ManualOrderOptionCall opLambda) { // is user public!
        xdoWithManualOrder(cMOO(opLambda));
    }

    // ===================================================================================
    //                                                                    Small Adjustment
    //                                                                    ================
    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    protected StampsCB newMyCB() {
        return new StampsCB();
    }
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabUDT() { return Date.class.getName(); }
    protected String xabCQ() { return StampsCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSLCS() { return HpSLCSetupper.class.getName(); }
    protected String xabSCP() { return SubQuery.class.getName(); }
}
