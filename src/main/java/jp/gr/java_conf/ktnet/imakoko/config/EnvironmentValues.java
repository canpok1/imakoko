package jp.gr.java_conf.ktnet.imakoko.config;

import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.URI;

/**
 * 環境設定を取得するためのクラスです.
 * @author tanabe
 *
 */
@Component
@ConfigurationProperties("db")
@Slf4j
@Data
public class EnvironmentValues {

  // DBのスキーマ名
  private static final String SCHEMA_NAME = "imakoko";
  
  // herokuで設定される接続値
  private String url;
  
  private boolean enableSsl;
  
  /**
   * JDBCのURLを取得します.
   * @return JDBCのURL.
   */
  public String getDatabaseJdbcUrl() {
    String param = "?currentSchema=" + SCHEMA_NAME;
    
    if (enableSsl) {
      param += "&sslmode=require";
    }
    
    if (url == null) {
      return null;
    }
    
    try {
      URI dbUri = new URI(url);
      
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("jdbc:postgresql://");
      stringBuilder.append(dbUri.getHost());
      stringBuilder.append(':');
      stringBuilder.append(dbUri.getPort());
      stringBuilder.append(dbUri.getPath());
      stringBuilder.append(param);
      
      return stringBuilder.toString();
    } catch (Exception e) {
      log.warn("例外発生", e);
      return null;
    }
  }
  
  /**
   * JDBCのユーザー名を取得します.
   * @return ユーザー名.
   */
  public String getDatabaseUsername() {
    try {
      URI dbUri = new URI(url);
      String[] splited = dbUri.getUserInfo().split(":");
      return (splited == null || splited.length < 1) ? "" : splited[0];
    } catch (Exception e) {
      log.warn("例外発生", e);
      return null;
    }
  }
  
  /**
   * JDBCのパスワードを取得します.
   * @return パスワード.
   */
  public String getDatabasePassword() {
    try {
      URI dbUri = new URI(url);
      String[] splited = dbUri.getUserInfo().split(":");
      return (splited == null || splited.length < 2) ? "" : splited[1];
    } catch (Exception e) {
      log.warn("例外発生", e);
      return null;
    }
  }
}
