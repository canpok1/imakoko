package jp.gr.java_conf.ktnet.imakoko.model.repository.mapper;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.StampSheet;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * stamp_sheetsテーブルからのレコード取得方法を定義するインターフェースです.
 * @author tanabe
 */
public interface StampSheetMapper {
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " created,"
      + " updated "
      + "FROM"
      + " imakoko.stamp_sheets")
  public List<StampSheet> findAll();
  
  /**
   * 指定のIDに一致するレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " created,"
      + " updated "
      + "FROM"
      + " imakoko.stamp_sheets "
      + "WHERE"
      + " id = #{id}")
  public StampSheet findById(@Param("id") final String id);

  
  @Insert(
      "INSERT INTO imakoko.stamp_sheets ("
      + " id,"
      + " created,"
      + " updated"
      + ") VALUES ("
      + " #{id},"
      + " NOW(),"
      + " NOW()"
      + ")")
  public int create(@Param("id") final String id);

  /**
   * レコードの更新日時を更新します.
   * @param id ID.
   * @return 更新したレコード数.
   */
  @Update(
      "UPDATE imakoko.stamp_sheets SET "
      + " updated = NOW() "
      + "WHERE"
      + " id = #{id}"
      )
  public int updateUpdated(@Param("id") final String id);
  
  
  @Select(
      "SELECT"
      + " id,"
      + " created,"
      + " updated "
      + "FROM imakoko.stamp_sheets sheets "
      + "WHERE"
      + " sheets.updated < (NOW() - CAST('${activeDateCount} days' AS INTERVAL))")
  public List<StampSheet> findInactiveRecord(@Param("activeDateCount") int activeDateCount);
  
  
  @Delete(
      "<script>"
      + "DELETE "
      +  "FROM imakoko.stamp_sheets "
      +  "WHERE imakoko.stamp_sheets.id IN"
      + "<foreach item='id' collection='idList' open='(' separator=',' close=')'>"
      +  "#{id}"
      + "</foreach>"
      + "</script>")
  public void deleteById(@Param("idList") final List<String> idList);
}
