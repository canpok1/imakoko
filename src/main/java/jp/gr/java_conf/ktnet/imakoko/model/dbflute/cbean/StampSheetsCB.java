package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.bs.BsStampSheetsCB;

/**
 * The condition-bean of stamp_sheets.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class StampSheetsCB extends BsStampSheetsCB {
}
