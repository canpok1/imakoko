package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean;

import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.bs.BsStampsCB;

/**
 * The condition-bean of stamps.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class StampsCB extends BsStampsCB {
}
