package jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.bs;

import java.util.Map;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.ciq.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.imakoko.model.dbflute.cbean.cq.*;

/**
 * The base condition-query of stamps.
 * @author DBFlute(AutoGenerator)
 */
public class BsStampsCQ extends AbstractBsStampsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected StampsCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsStampsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br>
     * {select ... from ... left outer join (select * from stamps) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public StampsCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected StampsCIQ xcreateCIQ() {
        StampsCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected StampsCIQ xnewCIQ() {
        return new StampsCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br>
     * {select ... from ... left outer join stamps on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public StampsCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        StampsCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    protected ConditionValue _id;
    public ConditionValue xdfgetId()
    { if (_id == null) { _id = nCV(); }
      return _id; }
    protected ConditionValue xgetCValueId() { return xdfgetId(); }

    /** 
     * Add order-by as ascend. <br>
     * id: {PK, NotNull, varchar(36)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Id_Asc() { regOBA("id"); return this; }

    /**
     * Add order-by as descend. <br>
     * id: {PK, NotNull, varchar(36)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Id_Desc() { regOBD("id"); return this; }

    protected ConditionValue _stampSheetId;
    public ConditionValue xdfgetStampSheetId()
    { if (_stampSheetId == null) { _stampSheetId = nCV(); }
      return _stampSheetId; }
    protected ConditionValue xgetCValueStampSheetId() { return xdfgetStampSheetId(); }

    /** 
     * Add order-by as ascend. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_StampSheetId_Asc() { regOBA("stamp_sheet_id"); return this; }

    /**
     * Add order-by as descend. <br>
     * stamp_sheet_id: {NotNull, varchar(36), FK to stamp_sheets}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_StampSheetId_Desc() { regOBD("stamp_sheet_id"); return this; }

    protected ConditionValue _name;
    public ConditionValue xdfgetName()
    { if (_name == null) { _name = nCV(); }
      return _name; }
    protected ConditionValue xgetCValueName() { return xdfgetName(); }

    /** 
     * Add order-by as ascend. <br>
     * name: {NotNull, varchar(64)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Name_Asc() { regOBA("name"); return this; }

    /**
     * Add order-by as descend. <br>
     * name: {NotNull, varchar(64)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Name_Desc() { regOBD("name"); return this; }

    protected ConditionValue _latitude;
    public ConditionValue xdfgetLatitude()
    { if (_latitude == null) { _latitude = nCV(); }
      return _latitude; }
    protected ConditionValue xgetCValueLatitude() { return xdfgetLatitude(); }

    /** 
     * Add order-by as ascend. <br>
     * latitude: {NotNull, text(2147483647)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Latitude_Asc() { regOBA("latitude"); return this; }

    /**
     * Add order-by as descend. <br>
     * latitude: {NotNull, text(2147483647)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Latitude_Desc() { regOBD("latitude"); return this; }

    protected ConditionValue _longitude;
    public ConditionValue xdfgetLongitude()
    { if (_longitude == null) { _longitude = nCV(); }
      return _longitude; }
    protected ConditionValue xgetCValueLongitude() { return xdfgetLongitude(); }

    /** 
     * Add order-by as ascend. <br>
     * longitude: {NotNull, text(2147483647)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Longitude_Asc() { regOBA("longitude"); return this; }

    /**
     * Add order-by as descend. <br>
     * longitude: {NotNull, text(2147483647)}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Longitude_Desc() { regOBD("longitude"); return this; }

    protected ConditionValue _created;
    public ConditionValue xdfgetCreated()
    { if (_created == null) { _created = nCV(); }
      return _created; }
    protected ConditionValue xgetCValueCreated() { return xdfgetCreated(); }

    /** 
     * Add order-by as ascend. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Created_Asc() { regOBA("created"); return this; }

    /**
     * Add order-by as descend. <br>
     * created: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Created_Desc() { regOBD("created"); return this; }

    protected ConditionValue _updated;
    public ConditionValue xdfgetUpdated()
    { if (_updated == null) { _updated = nCV(); }
      return _updated; }
    protected ConditionValue xgetCValueUpdated() { return xdfgetUpdated(); }

    /** 
     * Add order-by as ascend. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Updated_Asc() { regOBA("updated"); return this; }

    /**
     * Add order-by as descend. <br>
     * updated: {NotNull, timestamp(29, 6), default=[now()]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_Updated_Desc() { regOBD("updated"); return this; }

    protected ConditionValue _iconType;
    public ConditionValue xdfgetIconType()
    { if (_iconType == null) { _iconType = nCV(); }
      return _iconType; }
    protected ConditionValue xgetCValueIconType() { return xdfgetIconType(); }

    /** 
     * Add order-by as ascend. <br>
     * icon_type: {int4(10), default=[0]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_IconType_Asc() { regOBA("icon_type"); return this; }

    /**
     * Add order-by as descend. <br>
     * icon_type: {int4(10), default=[0]}
     * @return this. (NotNull)
     */
    public BsStampsCQ addOrderBy_IconType_Desc() { regOBD("icon_type"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsStampsCQ addSpecifiedDerivedOrderBy_Asc(String aliasName) { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsStampsCQ addSpecifiedDerivedOrderBy_Desc(String aliasName) { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    public void reflectRelationOnUnionQuery(ConditionQuery bqs, ConditionQuery uqs) {
        StampsCQ bq = (StampsCQ)bqs;
        StampsCQ uq = (StampsCQ)uqs;
        if (bq.hasConditionQueryStampSheets()) {
            uq.queryStampSheets().reflectRelationOnUnionQuery(bq.queryStampSheets(), uq.queryStampSheets());
        }
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    /**
     * Get the condition-query for relation table. <br>
     * stamp_sheets by my stamp_sheet_id, named 'stampSheets'.
     * @return The instance of condition-query. (NotNull)
     */
    public StampSheetsCQ queryStampSheets() {
        return xdfgetConditionQueryStampSheets();
    }
    public StampSheetsCQ xdfgetConditionQueryStampSheets() {
        String prop = "stampSheets";
        if (!xhasQueRlMap(prop)) { xregQueRl(prop, xcreateQueryStampSheets()); xsetupOuterJoinStampSheets(); }
        return xgetQueRlMap(prop);
    }
    protected StampSheetsCQ xcreateQueryStampSheets() {
        String nrp = xresolveNRP("stamps", "stampSheets"); String jan = xresolveJAN(nrp, xgetNNLvl());
        return xinitRelCQ(new StampSheetsCQ(this, xgetSqlClause(), jan, xgetNNLvl()), _baseCB, "stampSheets", nrp);
    }
    protected void xsetupOuterJoinStampSheets() { xregOutJo("stampSheets"); }
    public boolean hasConditionQueryStampSheets() { return xhasQueRlMap("stampSheets"); }

    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    public Map<String, StampsCQ> xdfgetScalarCondition() { return xgetSQueMap("scalarCondition"); }
    public String keepScalarCondition(StampsCQ sq) { return xkeepSQue("scalarCondition", sq); }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public Map<String, StampsCQ> xdfgetSpecifyMyselfDerived() { return xgetSQueMap("specifyMyselfDerived"); }
    public String keepSpecifyMyselfDerived(StampsCQ sq) { return xkeepSQue("specifyMyselfDerived", sq); }

    public Map<String, StampsCQ> xdfgetQueryMyselfDerived() { return xgetSQueMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerived(StampsCQ sq) { return xkeepSQue("queryMyselfDerived", sq); }
    public Map<String, Object> xdfgetQueryMyselfDerivedParameter() { return xgetSQuePmMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerivedParameter(Object pm) { return xkeepSQuePm("queryMyselfDerived", pm); }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, StampsCQ> _myselfExistsMap;
    public Map<String, StampsCQ> xdfgetMyselfExists() { return xgetSQueMap("myselfExists"); }
    public String keepMyselfExists(StampsCQ sq) { return xkeepSQue("myselfExists", sq); }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    public Map<String, StampsCQ> xdfgetMyselfInScope() { return xgetSQueMap("myselfInScope"); }
    public String keepMyselfInScope(StampsCQ sq) { return xkeepSQue("myselfInScope", sq); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return StampsCB.class.getName(); }
    protected String xCQ() { return StampsCQ.class.getName(); }
    protected String xCHp() { return HpQDRFunction.class.getName(); }
    protected String xCOp() { return ConditionOption.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
