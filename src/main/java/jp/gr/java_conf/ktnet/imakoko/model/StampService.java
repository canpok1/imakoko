package jp.gr.java_conf.ktnet.imakoko.model;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampRepository;
import jp.gr.java_conf.ktnet.imakoko.model.repository.StampSheetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * スタンプ関連の処理を行うサービスクラスです.
 * @author tanabe
 */
@Service
@Slf4j
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class StampService {
  
  @Autowired
  private StampRepository stampRepository;

  @Autowired
  private StampSheetRepository stampSheetRepository;
  
  /**
   * ID指定でスタンプ情報を取得します.
   * @param id ID.
   * @param stampSheetId スタンプシートID.
   * @return スタンプ情報. 見つからない場合はnull.
   */
  public Stamp findById(String id, String stampSheetId) {
    return stampRepository.findById(id, stampSheetId);
  }
  
  /**
   * スタンプ情報を新規作成、もしくは更新します.
   * IDに対応するレコードが見つからない場合は新規作成、見つかった場合は更新します.
   * @param record スタンプ情報.
   * @return 新規作成の場合は割り振られたID、更新の場合は更新レコードのID.
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public String createOrUpdate(Stamp record) {
    String id = record.getId();
    String stampSheetId = record.getStampSheetId();
    Stamp existRecord = findById(id, stampSheetId);
    
    String stampId = null;
    if (existRecord == null) {
      // 新規作成
      stampId = stampRepository.insert(record);
    } else {
      // 更新
      stampId = stampRepository.update(record);
    }
    
    stampSheetRepository.updateCreated(stampSheetId);
    
    return stampId;
  }

  /**
   * スタンプシートID指定でスタンプ情報を取得します.
   * @param stampSheetId スタンプシートID.
   * @return スタンプ情報. 見つからない場合は空のList.
   */
  public List<Stamp> findByStampSheetId(String stampSheetId) {
    return stampRepository.findByStampSheetId(stampSheetId);
  }
}
