package jp.gr.java_conf.ktnet.imakoko.controller.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import jp.gr.java_conf.ktnet.imakoko.model.StampService;
import jp.gr.java_conf.ktnet.imakoko.model.entity.api.ApiResponseTemplate;
import jp.gr.java_conf.ktnet.imakoko.model.entity.db.Stamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * スタンプAPIを定義するコントローラーです.
 * @author tanabe
 */
@RestController
@RequestMapping("/api/stamps")
@Slf4j
public class StampApiController {
  
  @Autowired
  private StampService stampService;
  
  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  private static class SaveStampParam {
    @ApiModelProperty(value = "更新対象のスタンプID.対象が見つからない時はスタンプを新規作成します.", required = false)
    @JsonProperty("id")
    private String id;
    
    @ApiModelProperty(value = "マップのID", required = true)
    @JsonProperty("stamp_sheet_id")
    private String stampSheetId;
    
    @ApiModelProperty(value = "作成者名", required = true)
    @JsonProperty("name")
    private String name;
    
    @ApiModelProperty(value = "緯度", required = true)
    @JsonProperty("latitude")
    private String latitude;
    
    @ApiModelProperty(value = "経度", required = true)
    @JsonProperty("longitude")
    private String longitude;
    
    @ApiModelProperty(value = "アイコン種別（0:いまここ, 1〜10:マーカー1〜10）", required = false)
    @JsonProperty("icon_type")
    private int iconType;
  }
  
  @Data
  @AllArgsConstructor
  private static class SaveStampResponseBody {
    @ApiModelProperty(value = "割り当てられたID", required = true)
    private final String id;
  }
  
  /**
   * スタンプを新しく生成もしくは更新します.
   * @param param パラメータ.
   * @param response レスポンス.
   * @return レスポンスのボディ.
   * @throws Exception 例外発生.
   */
  @RequestMapping(method = RequestMethod.POST)
  public ApiResponseTemplate<SaveStampResponseBody> saveStamp(
      @RequestBody final SaveStampParam param,
                   final HttpServletResponse response
  ) throws Exception {
    
    String id = stampService.createOrUpdate(new Stamp(
        param.getId(),
        param.getStampSheetId(),
        param.getName(),
        param.getLatitude(),
        param.getLongitude(),
        param.getIconType(),
        null,
        null
    ));
    
    SaveStampResponseBody responseBody = new SaveStampResponseBody(id);
    return new ApiResponseTemplate<SaveStampResponseBody>("OK", responseBody);
  }

  /**
   * 指定スタンプシート内の全スタンプを取得します.
   * @param stampSheetId スタンプシートID.
   * @param response レスポンス.
   * @return レスポンスのボディ.
   * @throws Exception 例外発生.
   */
  @RequestMapping(value = "/{stampSheetId}", method = RequestMethod.GET)
  public ApiResponseTemplate<List<Stamp>> getStamp(
      @PathVariable final String stampSheetId,
                   final HttpServletResponse response
  ) throws Exception {
    
    List<Stamp> stampList = stampService.findByStampSheetId(stampSheetId);
    
    return new ApiResponseTemplate<List<Stamp>>("OK", stampList);
  }
}
