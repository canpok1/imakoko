package jp.gr.java_conf.ktnet.imakoko.model.repository;

import jp.gr.java_conf.ktnet.imakoko.model.entity.db.StampSheet;

import java.util.List;

/**
 * stamp_sheetsテーブルの情報を操作するクラスです.
 * @author tanabe
 */
public interface StampSheetRepository {

  /**
   * 全レコードを取得します.
   * @return 全レコード. 見つからない場合は空のList.
   */
  List<StampSheet> findAll();

  /**
   * 指定IDのレコードを取得します.
   * @param id ID.
   * @return レコード. 見つからない場合はnull.
   */
  StampSheet findById(String id);

  /**
   * レコードを新規作成します.
   * @return 新規レコードに割り振られたID.
   */
  String create();

  /**
   * 更新日を更新します.
   * @param id 更新対象レコードのID.
   */
  void updateCreated(String id);

  /**
   * 無効なスタンプシートを取得します.
   * スタンプシートの更新日が有効日数より場合は無効と判断します.
   * 例)更新日が「2016/08/10 15:00:00」のスタンプシート。
   * 有効日数が1 → 2016/08/11 15:00:00を迎えると無効になります.
   * 有効日数が0 → すぐに無効になります.
   * @param activeDateCount スタンプの有効日数.
   * @return 無効なスタンプシート.
   */
  List<StampSheet> findInactiveRecord(int activeDateCount);

  /**
   * レコードを削除します.
   * @param idList 削除対象レコードのID.
   */
  void deleteById(List<String> idList);

}